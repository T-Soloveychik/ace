/**
 * Created by Created by PhpStorm.
 *
 * Коротко – пояснение
 *
 * Детальное описание
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 2014-08-03 01:52
 *
 * @copyright Timofey - 1september 2014
 */

var aceDivFor = document.getElementById( "aceForYandexSpellerButton" );

/**
 * <span class="ace-button" id="aceSendTextToYandexSpeller" title="Проверка орфографии через сервис Яндекса"
 * onclick="aceSendTextToYandexSpeller(event);">Яндекс.Spell</span>
 */

aceDivFor.appendChild( document.createTextNode( " | " ) );

var aceYandexSpellerButton = document.createElement( "span" );
aceYandexSpellerButton.id = "aceSendTextToYandexSpeller";
aceYandexSpellerButton.className = "ace-button";
aceYandexSpellerButton.title = "Проверка орфографии через сервис Яндекса";
aceYandexSpellerButton.innerHTML = "Яндекс.Spell";
aceYandexSpellerButton.onclick = aceSendTextToYandexSpeller;
aceYandexSpellerButton.ondblclick = aceSendTextToYandexSpeller;

//aceYandexSpellerButton.status = true;
//aceYandexSpellerButton.text = editor.getValue();
aceYandexSpellerButton.startText = editor.getValue();

aceDivFor.appendChild( aceYandexSpellerButton );

/**
 * Подготовить текст
 *
 * и параметры запроса
 *
 * к отправке Яндексу
 */
function acePrepareTextToYandexSpeller ()
	{
		var text = editor.getValue()
		/** Максимальная длина - 10`000 символов */
			, chunkLength = 10000
//			, chunkLength = 75
		/** Разделитель новой строки */
			, EOL = editor.getSession().getDocument().getNewLineCharacter()
			, start = 0
			, length = 0
			, row = 0
			, wrapped = false
		/** Массив частей */
			, parts = {}
			, part
		/** Reg новой строки */
			, regEOL = new RegExp( EOL , "mig" )
			, splittersVar = [
				, " "
				, ","
				, "."
				, " "
			];


		/**
		 * Заменить в тексте все HTML-и и сущности
		 */
		text = acePrepareTextToYandexSpellerReplaceHTML( text );

		/**
		 * Пока стартовая позиция менее длины всего текста
		 */
		while ( start < text.length /*&& --count*/ )
			{
//				console.log('start: ' + start);
//				console.log('text.length: ' + text.length);



				/** Новая часть текста */
				part = text.substr( start , chunkLength );


//				console.log('start + length: ' + (start + length));

				/**
				 * Если конечная позиция части не является завершением текста
				 */
				if ( (start + part.length) < text.length )
					{


						/**
						 * Первая с конца позиция переносу строки
						 */
						length = part.lastIndexOf( EOL );


//						console.log('length: ' + (length));



						/**
						 * Если значение не найдено, то отметить, что строка будет разорвана по длине
						 */
						if ( length == -1 )
							{
								wrapped = true;

								/**
								 * Перебирать варианты разделителей
								 *
								 * Если будет найдено значение с конца, то завершить перебор
								 */
								for ( var i = 0 ; i < splittersVar.length ; i++ )
									{
										length = part.lastIndexOf( splittersVar[i] );

										/** Завершить, установив конец участка */
										if ( length != -1 )
											break;
									}

							}

						/**
						 * Если найдено значение, то взять от части часть, по позиции найднно разделителя
						 *
						 * +1, чтобы захватить найденную позицию
						 */
						if ( length != -1 )
							part = part.substr( 0 , ++length );



//						console.log('length: ' + (length));
					}


//				console.log('length: ' + length);
//
//				console.log('-----------');
//				console.log('-----------');
//				console.log('-----------');

//				console.log('text.length: ' + length);

				/** Сохранить в массив частей под значением строки начала части */
				parts[row] = part;



				/**
				 * Подготовка для следующей части
				 *
				 * Сместить вперёд позиция начала следующей части на длину данной части
				 */
				start += part.length;


				/**
				 * Если в значении строки было добавлено место разрыва,
				 *
				 * то взять только номер строки
				 */
				if ( /:/.test( row ) )
					row = +( row.split( ":" )[0] );

				/**
				 * Строка, с которой будет начата следующая часть
				 *
				 * Количество новых строк в данной части
				 */
				if ( part.match( regEOL ) )
					row += part.match( regEOL ).length;

				/**
				 * Если данная часть была разорвана, то отметить
				 *
				 * с какой позиции будет начинаться следующая часть
				 *
				 * и сбросить отметку о том, что данная часть была разбита
				 */
				if ( wrapped )
					{
						row = row + ":" + part.length;

						wrapped = false;
					}

			}

		console.log( parts );

		/** Кнопка активации проверки орфографии */
		var aceYandexSpellerButton = document.getElementById( "aceSendTextToYandexSpeller" );


		/**
		 * http://api.yandex.ru/speller/doc/dg/concepts/api-overview.xml
		 */
		return {
			storage : aceYandexSpellerButton ,
			url     : "https://speller.yandex.net/services/spellservice.json/checkText" ,
			method  : "POST" ,
			headers : {'Content-Type' : 'application/x-www-form-urlencoded'} ,
			onReady : aceCheckSendTextToYandexSpellerAnswer ,
			parts   : parts
		};

	}



/**
 * Параметры к запросу на проверку
 *
 * @param {string|string[]} option параметр или массив параметров
 *
 * <b>IGNORE_UPPERCASE</b>      - Пропускать слова, написанные заглавными буквами, например, "ВПК".
 * <b>NORE_DIGITS</b>           - Пропускать слова с цифрами, например, "авп17х4534".
 * <b>IGNORE_URLS</b>           - Пропускать интернет-адреса, почтовые адреса и имена файлов.
 * <b>FIND_REPEAT_WORDS</b>     - Подсвечивать повторы слов, идущие подряд. Например, "я полетел на на Кипр".
 * <b>IGNORE_LATIN</b>          - Пропускать слова, написанные латиницей, например, "madrid".
 * <b>NO_SUGGEST</b>            - Только проверять текст, не выдавая вариантов для замены.
 * <b>FLAG_LATIN</b>            - Отмечать слова, написанные латиницей, как ошибочные.
 * <b>BY_WORDS</b>              - Не использовать словарное окружение (контекст) при проверке. Опция полезна в случаях, когда на вход сервиса передается список отдельных слов.
 * <b>IGNORE_CAPITALIZATION</b> - Игнорировать неверное употребление ПРОПИСНЫХ/строчных букв, например, в слове "москва".
 * <b>IGNORE_ROMAN_NUMERALS</b> - Игнорировать римские цифры ("I, II, III, ...").
 *
 * @see http://api.yandex.ru/speller/doc/dg/reference/speller-options.xml
 */
function aceYandexSpellerOptions ( option )
	{
		var Options = {
			IGNORE_UPPERCASE        : 1    // Пропускать слова, написанные заглавными буквами, например, "ВПК".
			, IGNORE_DIGITS         : 2    // Пропускать слова с цифрами, например, "авп17х4534".
			, IGNORE_URLS           : 4    // Пропускать интернет-адреса, почтовые адреса и имена файлов.
			, FIND_REPEAT_WORDS     : 8    // Подсвечивать повторы слов, идущие подряд. Например, "я полетел на на Кипр".
			, IGNORE_LATIN          : 16   // Пропускать слова, написанные латиницей, например, "madrid".
			, NO_SUGGEST            : 32   // Только проверять текст, не выдавая вариантов для замены.
			, FLAG_LATIN            : 128  // Отмечать слова, написанные латиницей, как ошибочные.
			, BY_WORDS              : 256  // Не использовать словарное окружение (контекст) при проверке. Опция полезна в случаях, когда на вход сервиса передается список отдельных слов.
			, IGNORE_CAPITALIZATION : 512  // Игнорировать неверное употребление ПРОПИСНЫХ/строчных букв, например, в слове "москва".
			, IGNORE_ROMAN_NUMERALS : 2048 // Игнорировать римские цифры ("I, II, III, ...").
		};

		/**
		 * Если не массив, одна запись, то засунуть в массив
		 */
		if ( !option instanceof Array )
			{
				option = [option];
			}

		var sum = 0;

		for ( var i = 0 ; i < option.length ; i++ )
			{
				var obj = option[i];

				if ( Options.hasOwnProperty( obj ) )
					sum += Options[obj];
			}

		return sum;
	}





/**
 * Отправка текста в Yandex.Speller
 *
 * Если нажать Ctrl, то НЕ будет отправлен текст, но будут взяты отметки из памяти
 *
 * @param {event|object} ajaxObj
 *
 * @param {element} YaButton
 *
 * @return {false} - для того, чтобы не обрабатывалось нажатие на кнопку проверки
 */
function aceSendTextToYandexSpeller ( ajaxObj , YaButton )
	{

		var POST = true;


		/**
		 * Если нажатие на кнопку проверки
		 *
		 * Запущена проверка
		 */
		if ( ajaxObj instanceof Event )
			{
				ajaxObj.preventDefault();

				YaButton = ajaxObj.target;

				/**
				 * Если нажат Ctrl, то
				 * Сохранённый запрос, без AJAX!
				 */
				if ( ajaxObj.ctrlKey )
					POST = false;
				/**
				 * Иначе сбросить сохранённые ответы для запуска новых запросов
				 */
				else if ( !YaButton.partsMisspells || YaButton.status === false )
					YaButton.partsMisspells = {};


				/**
				 * Если в статусе проверки, то убрать и завершить выполнение
				 */
				if ( YaButton.status === true )
					return aceYandexSpellerRemoveMarks ( YaButton );


				/**
				 * Если ещё небыло проверок, до создать пустые хранилища
				 */
				if ( !YaButton.ajax )
					aceYandexSpellerRemoveMarks ( YaButton );


				YaButton.status = true;

				ajaxObj = acePrepareTextToYandexSpeller();

				ajaxObj.rows = [];
			}
		/**
		 * Если следующий проход по частям
		 */
		else YaButton = ajaxObj.storage;




		for ( var obj in ajaxObj.parts )
			{
//				console.log( 'obj: ' + obj );

				if ( ajaxObj.parts.hasOwnProperty( obj ) )
					{

//						console.log( ajaxObj.rows.indexOf(obj) );

						if ( ajaxObj.rows.indexOf( obj ) !== -1 )
							continue;

//						console.log( 'ajaxObj.row: ' + ajaxObj.row );

						ajaxObj.rows[ajaxObj.rows.length] = obj;

						ajaxObj.row = obj;

//						console.log( 'ajaxObj.part: ' + ajaxObj.parts[ajaxObj.row] );

						ajaxObj.data = "text=" + encodeURIComponent( ajaxObj.parts[ajaxObj.row] );

//						console.log( 'ajaxObj.parts[ajaxObj.row]' );
//						console.log( ajaxObj.parts[ajaxObj.row] );

						/**
						 * Добавить параметры проверки
						 */
						ajaxObj.data += "&options=" + aceYandexSpellerOptions( ['FIND_REPEAT_WORDS'/*, 'NO_SUGGEST'*/] );

//						ajaxObj.data += "&format=plain";
//						ajaxObj.data += "&format=html";

						/**
						 * Язык(и) проверки
						 */
						ajaxObj.data += "&lang=ru,en";


//						console.log( ajaxObj.parts );
//						console.log('ajaxObj.row: ' + ajaxObj.row);

						if ( POST )
							{

								AJAX( ajaxObj );

								break;

							}
						else
							{
								/**
								 * Сохранённый ответ для тестов
								 */
								var StoredResponse = eval( '([{"code":1,"pos":248,"row":16,"col":44,"len":8,"word":"\u0430\u0442\u0440\u0438\u0431\u0443\u0442\u0442","s":["\u0430\u0442\u0440\u0438\u0431\u0443\u0442"]},{"code":3,"pos":325,"row":23,"col":3,"len":6,"word":"\u041f\u0420\u0430\u0432\u0434\u0430","s":["\u041f\u0440\u0430\u0432\u0434\u0430"]},{"code":1,"pos":359,"row":27,"col":3,"len":21,"word":"\u042d\u043a\u0432\u0430\u043d\u0438\u0437\u0438\u0437\u0430\u0442\u043e\u0440\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435","s":[]},{"code":1,"pos":417,"row":31,"col":16,"len":5,"word":"memos","s":["mamas","memo"]},{"code":2,"pos":547,"row":39,"col":13,"len":2,"word":"\u043d\u0430","s":[]},{"code":1,"pos":571,"row":41,"col":10,"len":7,"word":"\u0430\u0448\u0438\u0431\u043a\u043e\u0439","s":["\u043e\u0448\u0438\u0431\u043a\u043e\u0439"]}])' );


								/**
								 * Если сохранены ошибки
								 */
								if ( YaButton.partsMisspells )
									{
										for ( var row in YaButton.partsMisspells )
											{
												if ( !YaButton.partsMisspells.hasOwnProperty( row ) )
													continue;

												aceMarkTextByYandexSpellerAnswer(  YaButton.partsMisspells[row] , row , YaButton )
											}
									}
								else aceMarkTextByYandexSpellerAnswer(  StoredResponse , '0' , YaButton );
//								else YaButton.text = editor.getValue();

								/**
								 * Обновить статус
								 */
								documentStatus();
							}
					}
			}

		return false;

	}


function aceCheckSendTextToYandexSpellerAnswer ( ajaxObj )
	{
		var responseArray = eval( "(" + ajaxObj.ajax.responseText + ")" );

		var YaButton = ajaxObj.storage;

		if ( responseArray )
			{
				documentStatus();

				YaButton.partsMisspells[ajaxObj.row] = responseArray;

				aceMarkTextByYandexSpellerAnswer( responseArray , ajaxObj.row , YaButton );

				/**
				 * Добавить функцию сравнения для вывода сообщений об ошибках
				 */
				//editor.on("changeSelection", function() {
				//	aceYandexSpellMisspelled();
				//});
			}


//		editor.getSelection().shiftSelection()

		/**
		 * Пока не будут отправлены все части теста, повторять
		 */
		if ( aceSendTextToYandexSpeller( ajaxObj , YaButton ) )
			{
				/**
				 * Обновить статус
				 */
				documentStatus();
			}
		else
			{
				/**
				 * Если нет ошибок, то запомнить текст и указать, что проверка выключена
				 */
				if ( YaButton.markers.length == 0 )
					{
						YaButton.status = false;

						YaButton.text = editor.getValue();

						/**
						 * Запомнить на кнопке сессии, что текст без ошибок
						 */
						YaButton.sessionButton.spelled = editor.getValue();

						documentStatus("Ошибки отсутсвуют");
					}
			}
	}


function aceYandexSpellerRemoveMarks ( YaButton )
	{

		YaButton.status = false;


		/**
		 * Очистить сохранённое количество ошибок
		 */
		YaButton.misspells = {};


		var session = ace.edit( editor ).getSession();

		/**
		 * Убрать отметка на поле номеров строк
		 */
		if ( YaButton.decoredGutters )
			for ( var d = 0 ; d < YaButton.decoredGutters.length ; d++ )
				{
					session.removeGutterDecoration( YaButton.decoredGutters[d][0] , YaButton.decoredGutters[d][1] );
				}

		/**
		 * Очистить сохранённые значения об отметках
		 */
		YaButton.decoredGutters = [];




		/**
		 * Убрать Все выделения
		 */
		if ( YaButton.markers )
			for ( var m = 0 ; m < YaButton.markers.length ; m++ )
				{
					session.removeMarker( YaButton.markers[m] );
				}

		YaButton.markers = [];


		documentStatus();

	}


function aceYandexSpellerGetMarksNames ()
	{
		return [
			"yandex_misspelled"
			, "yandex_misspelled_double"
			, "yandex_misspelled_cap"
			, "yandex_misspelled_many"
		];
	}

/**
 * 1 - ERROR_UNKNOWN_WORD    Слова нет в словаре.
 * 2 - ERROR_REPEAT_WORD    Повтор слова.
 * 3 - ERROR_CAPITALIZATION    Неверное употребление прописных и строчных букв.
 * 4 - ERROR_TOO_MANY_ERRORS    Текст содержит слишком много ошибок. При этом приложение может отправить Яндекс.Спеллеру оставшийся непроверенным текст в следующем запросе.
 *
 * @param {array} responseArray массив с объектами описывающими ошибки
 *
 * @param {int|string} nowRow строка начала отсчёта (если несколько частей по 10 тыс символов)
 *
 * @param {element} YaButton массив хранящий параметры запросов
 */
function aceMarkTextByYandexSpellerAnswer ( responseArray , nowRow , YaButton )
	{
		var mistakes = aceYandexSpellerGetMarksNames();

		var session = ace.edit( editor ).getSession();

		var Range = ace.require( 'ace/range' ).Range;



//		console.log( 'nowRow:' + nowRow );
//		console.log( YaButton.ajaxObj.parts[nowRow] );
//		console.log( 'responseArray: ' );
//		console.log( responseArray );


		var strike = nowRow.match( /(\d+)(?:(?::)(\d+))?/ );

//		console.log( 'strike' );
//		console.log( strike );

		var startRow = +strike[1];

		var startCol = 0;

		if ( strike[2] )
			startCol = +strike[2];


		/**
		 * Перебор переданных в ответе значений
		 */
		for ( var r = 0 ; r < responseArray.length ; r++ )
			{

				var code = --responseArray[r].code;

				/**
				 * Если не нулевая строка, то сбросить значение смещения в строке
				 */
				if ( startCol && responseArray[r].row )
					startCol = 0;

				/** Строка + строка обрабатываемой части */
//				console.log('responseArray[r].row: ' + responseArray[r].row);
				var row = +responseArray[r].row + startRow;
//				console.log( 'row: ' + row );


				/** Начальная позиция в строке */
//				console.log('responseArray[r].col: ' + responseArray[r].col);
				var col = +responseArray[r].col + startCol;
//				console.log( 'col: ' + col );


				/** Создать диапазон */
				var end = col + +responseArray[r].len;
//				console.log( 'end: ' + end );


				/** Позиция завершения выделения */
				var range = new Range( row , col , row , end );


//				console.log( range );

//				var words = ;



				/** Запомнить параметры выделения */
				YaButton.decoredGutters.push( [row, mistakes[code]] );

				/** Добавить выделения на нумерации строк */
				session.addGutterDecoration( row , mistakes[code] );




				/**
				 * Добавить выделения в текст
				 */
				YaButton.markers[YaButton.markers.length]
					= session.addMarker( range , mistakes[code] + " test" , "typo" , false );



				/**
				 * Записать данные об ошибке
				 */
				if ( !YaButton.misspells[row] )
					YaButton.misspells[row] = [];

				YaButton.misspells[row][YaButton.misspells[row].length]
					= [range , mistakes[code] , responseArray[r].s,YaButton.markers[YaButton.markers.length]];
			}
	}
