/**
 * Created by Timofey on 2014-06-26 04:58
 */

/**
 * @name getXmlHttp
 *
 * @function (@field|@event)
 *
 * {@link getXmlHttp}
 * @borrows getXmlHttp
 *
 * @see JSDocs <a href="http://usejsdoc.org/">http://usejsdoc.org/</a>
 * @see JSDocs <a href="http://ru.wikipedia.org/wiki/JSDoc">wiki/JSDoc</a>
 *
 * \@type http code response
 * @returns {boolean|string}
 *
 * @file This is my cool script.
 * @copyright Timofey - 1september 2014
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 2014-07-28 18:50
 *
 * */
function getXmlHttp () {
	var xmlhttp;
	try
		{
			xmlhttp = new ActiveXObject( "Msxml2.XMLHTTP" );
		}
	catch ( e )
		{
			try
				{
					xmlhttp = new ActiveXObject( "Microsoft.XMLHTTP" );
				}
			catch ( E )
				{
					xmlhttp = false;
				}
		}
	if ( !xmlhttp && typeof XMLHttpRequest != 'undefined' )
		{
			xmlhttp = new XMLHttpRequest();
		}
	return xmlhttp;
}



/**
 * @requires getXmlHttp()
 *
 * Выполнение AJAX (XMLHttpRequest) запроса.
 *
 * По параметрам выполнение запроса с передачей в @obj["storage"] статусов и полученного ответа.
 * По завершению обработки передаёт в указанную функцию результат ответе
 *
 * @example
 * // Три обязательных параметра - url,storage,onReady
 * var <b>requestObj</b> = {
 * 		storage : aceSaveButton    // {object} DOM объект в который будут передаваться параметры
 * 		,url : "save.php" // {object} адрес для запроса
 * 		,method : "POST"  // {string} метод запроса
 * 		,data : data      // {object} данные для передачи в POST
 * 		,headers : {
 * 						"x-ps-announce-saved":textForCheck
 * 						,'Content-Type':'application/x-www-form-urlencoded'
 * 					} // {object} заголовки
 * 		,onReady : checkSavingAnswer // {object} функция, которой будет передан результат
 * 		,onStatusChange : checkSavingAnswer // {object} функция, которой будет передан результат
 * 	};
 * AJAX( requestObj )
 * _____________________
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @param {{}}              obj Объект с параметрами запроса - new Object {}
 *
 * @param  {string}         obj.url url для запроса;
 * @param  {element}        obj.storage объект для хранения параметров;
 * @param  {function}       obj.onReady функция, которая будет выполнена после получения ответа на запрос:
 * 200 - {boolean} true;
 * 403 - {string} "403 - Нужно перезагрузиться и авторизоваться";
 * XXX - {string} "XXX - Ошибка ответа !!!";
 * @param  {object}         [obj.ajax] хранилище уж запущенного запроса;
 * @param  {string}         [obj.method] метод запроса GET/POST - по умолчанию - GET;
 * @param  {string|object|} [obj.data] данные для передачи;
 * @param  {object}         [obj.headers] ассоциативный массив (объект) с заголовками и их значениями;
 * @param  {function}       [obj.onError] функция, которая будет выполнена если ответ не 200;
 * @param  {function}       [obj.onAbort] @callback функция, которая будет выполнятся при остановке запроса;
 * @param  {function}       [obj.onStatusChange] @callback функция, которая будет выполнятся при изменении статуса обработки запроса, если не передана, то сообщения вывод в консоль console.log();
 *
 * Возвращаемые параметры:
 * @param  {boolean|string} [obj.responseStatus] текст ответа при НЕ 200, если 200, то true;
 * @param  {boolean}        [obj.authorization] отметка о том, что нужно авторизоваться;
 *
 * @returns {function|boolean} при ошибках возвращает alert с сообщением без ошибок - true
 *
 * @see getXmlHttp()
 *
 * @version 2014-08-1 5:04
 */
function AJAX ( obj ) {

	if ( obj.storage && obj.storage.ajaxInProgress === true )
		{
			obj.storage.ajaxObj.onAbort();
			return false;
		}

	/**
	 * Если объект
	 */
	if ( typeof obj == "object" )
		{

			if ( obj.elem )
				obj.storage = obj.elem;

			if ( obj.functionOnSuccess )
				obj.onReady = obj.functionOnSuccess;

			if ( obj.functionOnStatusChange )
				obj.onStatusChange = obj.functionOnStatusChange;

			/**
			 * Если нет обязательных параметров - alert
			 */
			if ( !obj.url || !obj.storage || !obj.onReady )
				return alert( "Нужны параметры: url,storage,onReady[,method][,headers][,data]" );
		}
	else
		return alert( "Для выполнения запроса нужен Object: {url,storage,onReady[,method][,headers][,data]}" );


	var storage = obj.storage;


	/**
	 * Если не передана функция обработки статуса, то выводить в console
	 */
	if ( !obj.onStatusChange /* instanceof Function */ )
		obj.onStatusChange = function ( value ) { return console.log( value ) };

	/**
	 * Если не передана функция обработки ошибки, то выводить в console
	 */
	if ( !obj.onError /* instanceof Function */ )
		obj.onError = function ( value ) { return console.log( value ) };

	/**
	 * Если не передана функция обработки ошибки, то выводить в console
	 */
	if ( !obj.onError /* instanceof Function */ )
		obj.onError = function ( value ) { return console.log( value ) };

	/**
	 * Если не передана функция обработки отмены, сообщить в консоль
	 */
	if ( !obj.onAbort /* instanceof Function */ )
		obj.onAbort = function ( value ) {
			value.ajax.abort();
			value.ajaxInProgress = false;
			return alert( "Запрос отменён" );
		};


	// Если не указан метод, то поставить GET:
	!obj.method ? obj.method = "GET" : "";




	// (1) создать объект для запроса к серверу
	obj.ajax = getXmlHttp();



	// После запроса изменения статуса ответа сервера:
	obj.ajax.onreadystatechange = function () {
		obj.onStatusChange( obj.ajax.statusText );// показать статус (Not Found, ОК..)

		// Если всё готово:
		if ( obj.ajax.readyState == 4 )
			{
				// Убрать .ajaxInProgress:
				storage.ajaxInProgress = false;

				// Для возврата результата:
				var responseStatus = "Не получилось обработать ответ";

				var httpStatus = obj.ajax.status;

				/** Сохранить ajax в выбранном хранилище */
				storage.ajax = obj.ajax;

				/** Сохранить ajax статус в объекте */
				storage.ajaxObj = obj;

				// если статус 200 (ОК):
				if ( httpStatus == 200 )
					responseStatus = true;

				// Если не 200 !!!:
				else
					{
						// Если 403 - вышло времени авторизации:
						if ( httpStatus == 403 || /Forbidden/i.test( obj.ajax.responseText ) )
							{
								responseStatus = httpStatus + " - Нужно перезагрузиться и авторизоваться";

								obj.authorization = true;
							}
						// Ошибка:
						else responseStatus = httpStatus + " - Ошибка ответа !!!";


					}

				/** Сохранить Ответ обработки запроса */
				obj.responseStatus = responseStatus;

				/**
				 * Запустить переданную функцию передав в ей объект,
				 * если при обработке запроса была ошибка onError,
				 * иначе onReady
				 */
				if ( responseStatus !== true )
					obj.onError( obj );
				else
					obj.onReady( obj );
			}
	};

	// (3) задать адрес подключения
	obj.ajax.open( obj.method , obj.url , true );

	// Есть есть доп. заголовки, то установить:
	for ( var header in obj.headers )
		{
			// Если нет такого параметра, то пропустить:
			if ( !obj.headers.hasOwnProperty( header ) )
				continue;

			obj.ajax.setRequestHeader( header , obj.headers[header] );
		}





	// объект запроса подготовлен: указан адрес и создана функция onreadystatechange
	// для обработки ответа сервера
	// (4)
	obj.ajax.send( obj.data ); // отослать запрос



	// Отметить, что идёт запрос:
	storage.ajaxInProgress = true;



	// (5)
	// Отметить в статус баре, что идёт обработка запроса:
	obj.onStatusChange( "Ожидание ответа сервера..." );


	return true;
}




function getAjaxParams ( data , elem , urlAdd ) {

	/**
	 * Если открыто дополнительное окно для авторизации, то закрыть его
	 */
	if ( window.authorizationWindow || window.closed )
		window.authorizationWindow.close();

	var text = [];
	for ( var f in data )
		{
			if ( data.hasOwnProperty( f ) )
				text.push( f + "=" + encodeURIComponent( data[f] ) );
		}

	var headers = {};

	headers['Content-Type'] = 'application/x-www-form-urlencoded';

	/** AJAX - XMLHttpRequest */
	headers['X-Requested-With'] = 'XMLHttpRequest';


	var onStatusChange = function ( value ) {
		if ( value == "OK" )
			removeClass( elem , 'AJAX' );
		//elem.parentNode.getElementsByTagName( 'input' )[0].value = value;
	};


	var onReady = function ( obj ) {
		//removeClass( elem , 'AJAX' );
		//elem.parentNode.getElementsByTagName( 'textarea' )[0].value = obj.ajax.responseText;
	};

	/**
	 * Если ошибка - то alert
	 */
	var onError = function ( value ) {
		removeClass( elem , 'AJAX' );

		if ( elem.ajaxObj.authorization || /304/.test(elem.ajaxObj.responseStatus) )
			window.authorizationWindow = window.open( "//" + location.host , "authorization" , "width=750, height=450" );

		else
			alert(
				"ОШИБКА!"
				+ "\n"
				+ "\n"
				+ elem.ajaxObj.responseStatus
				+ "\n"
				+ "\n"
				+ elem.ajax.responseText
			);
	};


	/**
	 * Если ошибка - то alert
	 */
	var onAbort = function () {
		if ( confirm( "Остановить запрос «" + elem.innerHTML + "»?" ) )
			{
				removeClass( elem , 'AJAX' );
				elem.ajaxObj.onError = function ( value ) { return console.log( value ) };
				elem.ajax.abort();
				elem.ajaxInProgress = false;
				return alert( "Запрос отменён" );
			}
		else return alert( "Продолжаем ждать ответа" );
	};


	elem.ajaxObj = {
		'url'            : (urlAdd ? urlAdd : "") + "post/" ,
		'method'         : 'POST' ,
		'data'           : text.join( "&" ) ,
		'headers'        : headers ,
		'storage'        : elem ,
		'onStatusChange' : onStatusChange ,
		'onReady'        : onReady ,
		'onError'        : onError ,
		'onAbort'        : onAbort
	};

	elem.data = data;


	return elem.ajaxObj;
}
