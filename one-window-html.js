/**
* Created by Created by PhpStorm.
*
* @file This is my cool script.
*
* Детальное описание
*
* @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
*
* @version 2014-08-12 0:36
*
* @copyright Timofey - 1september 2014
*/


function aceAddHTML()
	{

		var putTo = document.getElementById( "aceEditorWindow" ) || document.getElementsByTagName( "body" )[0];

		putTo.innerHTML += '\
\
		<!--	ACE editor -->\
			<div id="aceEditorDiv">\
\
				<hr>\
\
				<div id="aceEditorButtons">\
\
					<span class="ace-button" title="Ctrl+Alt+M" onclick="editor.showSettingsMenu();">Настройки</span>\
\
					| <span class="ace-button ace_writers" title="Ctrl+Alt+H" onclick="editor.showKeyboardShortcuts()">Сочетания клавиш</span>\
\
					|\
					<span class="ace-button" title="с проверкой правильности HTML кода"\
					onclick="editor.setOption(\'useWorker\',\'true\'); editor.getSession().setMode(\'ace/mode/html\');">HTML</span><!--\
					-->/<!--\
					--><span class="ace-button" title="БЕЗ проверки правильности HTML кода"\
									onclick="editor.setOption(\'useWorker\',\'false\'); editor.getSession().setMode(\'ace/mode/smarty\');">SMARTY</span><!--\
					-->/<!--\
					--><span class="ace-button" title="с проверкой JSON"\
								onclick="editor.setOption(\'useWorker\',\'true\'); editor.getSession().setMode(\'ace/mode/json\');">JSON</span><!--\
					-->/<!--\
					--><span class="ace-button" title="SQL"\
								onclick="editor.setOption(\'useWorker\',\'true\'); editor.getSession().setMode(\'ace/mode/sql\');">SQL</span><!--\
					-->/<!--\
					--><span class="ace-button" title="MySQL"\
								onclick="editor.setOption(\'useWorker\',\'true\'); editor.getSession().setMode(\'ace/mode/mysql\');">MySQL</span>\
\
					| <span class="ace-button" title="Переносить длинные строки"\
					onclick="wrapUnWrapLongLines();">↵</span>\
\
					| <span class="ace-button" title="Ctrl+Alt+P – скрыть/отобразить непечатные"\
					onclick="showHideUnprinted();">¶</span>\
					|\
						<span id="aceDocReadOnly1" class="ace-button ace-readOnly"></span>\
\
\
					<div style=\'float:right;\'>\
						<label for="themeEl">Подсветка</label>\
						<select id="themeEl" size="1"></select>\
					</div>\
				</div>\
\
\
				<hr>\
\
\
				<div id="aceEditorMacrosButtons" class="ace_writers">\
\
					<span class="ace-button" onclick="aceReplaceSpacesToTabsAndBack()">TABs↹Spaces</span>\
					| <span class="ace-button" onclick="showSearchBox();setSearchBoxToRegExp();">Лишние пробелы</span>\
					| <span class="ace-button" onclick="removeUselessLines()">Убрать пустые строки</span>\
						<!--			| <span class="ace-button" onclick="tabsToSpaces()">TABs>Spaces</span>-->\
						<!--			| <span class="ace-button" onclick="spacesToTabs()">Spaces>TABs</span>-->\
					| <span class="ace-button" title="Ctrl+Alt+E – записать macros" onclick="startStopRecMacros()"><span id="aceMacrosRec"></span> macros</span>\
					| <span class="ace-button" title="Ctrl+Shift+E (Command+Shift+E) – выполнить macros" onclick="replayMacros(event); return false;">► macros</span>\
\
				</div>\
\
\
				<hr>\
\
\
				<div>\
					<div id="aceEditorHelpButtons">\
						<span class="ace-button" title="Esc – fullWindowMode" onclick="fullWindowMode();">◰</span>\
\
						<span class="ace_writers">\
\
							<span style="margin: 0 1em;"></span>\
\
								Alt+\
							| <span class="ace-button" title="Alt+A" onclick="addLineTag(\'a\');" Style="text-decoration: underline;"><a href="#">A</a></span>\
							| <span class="ace-button" title="Alt+B" onclick="addLineTag(\'b\');" Style="font-weight: 700;">B</span>\
							| <span class="ace-button" title="Alt+I" onclick="addLineTag(\'i\');" Style="font-style: italic;">I</span>\
							| <span class="ace-button" title="Alt+U" onclick="addLineTag(\'u\');" Style="text-decoration: underline;">U</span>\
							| <span class="ace-button" title="Alt+T - добавит или обернуть в строчный тег" onclick="addLineTag();">inLine</span>\
\
								<span style="margin: 0 1em;"></span>\
\
								<span class="ace-button" title="Alt+P" onclick="addBlockTag(\'p\');">P</span>\
							| <span class="ace-button" title="Alt+D" onclick="addBlockTag(\'div\');">DIV</span>\
							| <span class="ace-button" title="Alt+Shift+u – UnOrdered List" onclick="addBlockTag(\'ul\');">UL</span>\
							| <span class="ace-button" title="Alt+Shift+o – Ordered List" onclick="addBlockTag(\'ol\');">OL</span>\
							| <span class="ace-button" title="Alt+L" onclick="addBlockTag(\'li\');">LI</span>\
\
							<span style="margin: 0 1em;"></span>\
\
						</span>\
\
						<span class="ace-button" title="Alt+Shift+L – скрыть блок/выделенный фрагмент" onclick="editor.getSession().toggleFold();">Скрыть</span>\
\
						<div style="float: right; padding-left: 1.5em;">\
							<span class="ace-button ace-resize-button" title="Автоматически задавать высоту по высоте содержания" onclick="aceAutoSizeOnOff();">⇅</span>\
						</div>\
\
						<hr>\
\
					</div>\
				</div>\
\
\
\
\
\
			<!--	Редактор	-->\
				<div>\
\
					<div class="ace_editor_background" onclick="fullWindowMode()"></div>\
					<pre id="aceEditorMainWindow"></pre>\
\
				</div>\
\
\
\
\
\
				<hr>\
\
			<!--	Статусная строка	-->\
				<div>\
					<div id="aceStatusBar">\
						<a style="margin: 0 0.3em;" title="Ace is an embeddable code editor written in JavaScript" target="_blank" href="http://ace.c9.io/#nav=about"><img id="AceEditorICON" src="' + aceJSurl + '/ace-builds/demo/bookmarklet/images/logo_half.png"></a>\
						|\
							<span id="aceDocReadOnly2" class="ace-readOnly"></span>\
						|\
							<span id="aceDocStatus"></span>\
							<span id="aceForSaveButton"></span>\
							<span id="aceForTipographButton"></span>\
							<span id="aceForYandexSpellerButton"></span>\
\
						<div style="float: right; padding-left: 1em;">\
							<span class="ace-button ace-resize-button" title="Автоматически задавать высоту по высоте содержания" onclick="aceAutoSizeOnOff();">⇅</span>\
						</div>\
					</div>\
				</div>\
\
\
				<hr>\
\
\
				<div id="helpInfo">\
\
					<p style="cursor: pointer;" onclick="addRemoveClass(this,\'show\')">\
						<span style="border-bottom: 1px dashed;">Возможности редактора</span>\
					</p>\
\
					<ol>\
						<li style="margin-top:1em;">\
							Список сочетания клавиш – Ctrl+Alt+h (Command+Alt+h)\
							(<a href="https://github.com/ajaxorg/ace/wiki/Default-Keyboard-Shortcuts">клавиш</a>)\
						</li>\
						<li style="margin-top:1em;">\
							Развернуть/свернуть окно редактора до размеров окна –  Esc\
						</li>\
						<li style="margin-top:1em;">\
							Отобразить скрыть непечатные символы – Ctrl+Alt+P (Command+Alt+P) – <span class="ace-button" onclick="showHideUnprinted();">¶</span>\
						</li>\
						<li style="margin-top:1em;">\
							Поиска текста и замена – Ctrl+H (Command+Option+F) – активировав режим поиска по регулярному выражению можно выделить и убрать лишние пробелы <span id="aceAdditionalButtonToSelectUselessSpaces" class="ace-button" onclick="showSearchBox();setSearchBoxToRegExp();"></span>\
\
						</li>\
						<li style="margin-top:1em;">\
							Подключён режим <a href="http://docs.emmet.io/">Emmet</a> – <a href="http://docs.emmet.io/abbreviations/syntax/">Синтаксис</a>:\
								<pre style="font-size: 1.5em; font-weight: 900; white-space: pre-line;">\
								ul>li.listStyle[style=margin-top:1em;]*5>b&#123;текст&#125;\
								</pre>\
							и нажать TAB (в режиме <span class="ace-button" onclick="editor.getSession().setMode(\'ace/mode/html\');">HTML</span>)\
						</li>\
						<li style="margin-top:1em;">\
							Перейти к ошибкам в синтаксисе – Alt+E (для корректной проверки должен быть выбран соответствующий коду синтаксис)\
						</li>\
						<li style="margin-top:1em;">\
							Завершение текста – Ctrl+Space (Command+Space)\
						</li>\
						<li style="margin-top:1em;">\
							Выделить текст по столбцам – Alt+выделение курсором мыши / Alt+Shit+кликнуть в нужном месте\
						</li>\
						<li style="margin-top:1em;">\
							Перемещение выделенных строк – Alt+стрелки вверх/вниз\
						</li>\
						<li style="margin-top:1em;">\
							Дублирование выделенных строк – Alt+Shift+стрелки вверх/вниз (Command+Option+Up/Down)\
						</li>\
						<li style="margin-top:1em;">\
							Редактирование в нескольких местах – Ctrl+кликнуть в нужных местах\
						</li>\
						<li style="margin-top:1em;">\
							Скрыть/отобразить выделенный фрагмент – Alt+Shift+L / скрыть всё что вне зоны курсора – Alt+0 (Command+Option+0)\
						</li>\
						<li style="margin-top:1em;">\
							Можно включить режим только для чтения, изображение – <span id="aceDocReadOnly3" class="ace-readOnly"></span> –  в статусной строке у основания окна – Alt+R (только активация) – в режиме только чтения, сочетания клавиш не работают\
						</li>\
					</ol>\
\
				</div>\
\
			</div>\
';

	}
