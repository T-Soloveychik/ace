/**
 * Created by Created by PhpStorm.
 *
 * @file This is my cool script.
 *
 * Коротко – пояснение
 *
 * Детальное описание
 *
 * @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
 *
 * @version 2014-06-29 11:55
 *
 * @copyright Timofey - 1september 2014
 */


function selectUselessSpaces ()
	{
		// В RegExp:
		var regString = getStringForRegExpUselessSpaces();
		var find = new RegExp( regString , "mig" );
		// Выделить всё:
		return editor.findAll( find , {
			wrap : true , wholeWord : false , regExp : true
		} , true );
	}


document.getElementById( "aceAdditionalButtonToSelectUselessSpaces" ).innerHTML = getStringForRegExpUselessSpaces();




/**
 * @description Отобразить окно поиска
 * взял с stackoverflow – <a href="http://stackoverflow.com/questions/17633324/ace-editor-change-ctrlh-keybinding">wiki/JSDoc</a>
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 01.07.14 0:20
 *
 * @see <a href="http://stackoverflow.com/questions/17633324/ace-editor-change-ctrlh-keybinding">stackoverflow</a>
 *
 * */
function showSearchBox ()
	{
		require( "ace/config" ).loadModule( "ace/ext/searchbox" , function ( e )
		{
			e.Search( editor , true );
			// take care of keybinding inside searchbox
			// this is too hacky :(
			var kb = editor.searchBox.$searchBarKb;
			command = kb.commands["Ctrl-f|Command-f|Ctrl-H|Command-Option-F"];
			if ( command.bindKey.indexOf( "Ctrl-R" ) == -1 )
				{
					command.bindKey += "|Ctrl-R";
					kb.addCommand( command );
				}
		} );
	}



/**
 * @description Добавить в
 * взял с <a href="http://stackoverflow.com/questions/17633324/ace-editor-change-ctrlh-keybinding">stackoverflow</a>
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 01.07.14 0:20
 *
 * @see <a href="http://stackoverflow.com/questions/17633324/ace-editor-change-ctrlh-keybinding">stackoverflow</a>
 *
 * */
function setSearchBoxToRegExp ()
	{
		window.setTimeout( function ()
						   {
							   editor.searchBox.regExpOption.checked = true;
							   editor.searchBox.searchInput.value = getStringForRegExpUselessSpaces();
							   editor.searchBox.replaceInput.value = "$1";
							   editor.searchBox.$syncOptions();
						   } , 350 );
	}



/**
 * @description Ace - заменить пробелы на tab-ы и обратно
 *
 * @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
 *
 * @version 2014-08-3 20:00
 *
 * */
function aceReplaceSpacesToTabsAndBack ()
	{
		// Проверка режима только для чтения:
		if ( checkReadOnly() )
			return false;

		if ( editor.getSession().getUseSoftTabs() )
			{
				// Установить использование TAB-ов для отступов:
				editor.getSession().setUseSoftTabs( false );
				// Отступы:
				var tab = editor.getSession().getTabString();

				// Размер Отступа:
				var tabSize = editor.getSession().getTabSize();

				// В RegExp:
				var find = new RegExp( " {2," + tabSize + "}" , "mig" );
			}
		else
			{
				// Установить использование TAB-ов для отступов:
				editor.getSession().setUseSoftTabs( true );
				// Отступы:
				var tab = editor.getSession().getTabString();

				// В RegExp:
				var find = new RegExp( "\t" , "mig" );
			}

		// Выделить всё:
		editor.find( find , {
			wrap : true , wholeWord : false , regExp : true
		} , true );

		// Заменить:
		editor.replaceAll( tab );

		return editor.focus();
	}


/**
 * @description Ace - убирает пустые строки
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 2014-08-2 23:48
 */
function removeUselessLines ()
	{
		// Проверка режима только для чтения:
		if ( checkReadOnly() )
			return false;

		var searchOptions = {
			backwards     : false ,
			wrap          : true ,
			caseSensitive : false ,
			wholeWord     : false ,
			skipCurrent   : false ,
			regExp        : true
		};

		var lastLine = editor.getSession().getDocument().getLength();

		console.log(editor.getSession().getDocument().getLength());

		var lastLineLength = editor.getSession().getDocument().getLine(lastLine).length;

		// Добавить в конце пустую строку:
		editor.getSession().getDocument().insertNewLine( {row : lastLine , column : lastLineLength} );

		editor.findAll( /^\s*$/mig , searchOptions );

		var finds = editor.getSelection().rangeList.ranges;

		if ( !finds )
			return false;

		for ( var rn = finds.length - 2 ; rn >= 0 ; rn-- )
			{
				var line = finds[rn].start.row;

				editor.getSession().getDocument().removeLines( line , line );
			}

		lastLine = editor.getSession().getDocument().getLength();

		/**
		 * Убрать выделение перейдя к последней строке
		 */
		var Range = ace.require( 'ace/range' ).Range;
		var range = {start : {row : lastLine , column : 0} , end : {row : lastLine , column : 0}};

//		editor.getSession().getSelection().setRange(range);
		editor.getSession().getSelection().setRange( new Range( 0 , 0 , 0 , 0 ) );

		return editor.focus();
	}


/**
 * @description Ace - начать/завершить запись macros
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 01.07.14 2:31
 *
 * */
function startStopRecMacros ()
	{
		// Проверка режима только для чтения:
		if ( checkReadOnly() )
			return false;

		// Найденное:
		editor.commands.toggleRecording( editor );

		return true;
	}


/**
 * @description Ace - начать/завершить запись macros
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 01.07.14 2:31
 *
 * */
function replayMacros ( e )
	{
		// Проверка режима только для чтения:
		if ( checkReadOnly() )
			return false;
		// Запустить записанный macros:
		editor.commands.replay( editor );

		e.preventDefault();
//			e.defaultPrevented();
		return false;
	}

