/**
 * Created by Timofey on 25.06.14.
 * @return {boolean}
 */


// Создать новую сессию:
// http://stackoverflow.com/questions/19049399/ace-editor-setting-new-session
var aceNewSession = function ( sesName , text , syntax ) {
	var aceSessionsDiv;

	if ( !window.aceSessions )
		{
			window.aceSessions = {};
			window.aceSessionButtons = {};

			aceSessionsDiv = document.createElement( "div" );
			aceSessionsDiv.id = "aceSessionsDiv";

			var helpButtons = document.getElementById( "aceEditorHelpButtons" );
			helpButtons.appendChild( aceSessionsDiv );
		}
	else
		aceSessionsDiv = document.getElementById( "aceSessionsDiv" );


	var number = 0;

	while ( window.aceSessions[sesName] )
		sesName = sesName + " - " + ++number;

	var sesButton = document.createElement( "span" );
	sesButton.innerHTML = sesName;
	sesButton.className = "ace-button ace-session";
	sesButton.onclick = aceLoadSession;
	sesButton.startText = text;

	aceSessionsDiv.appendChild( sesButton );

	window.aceSessionButtons[sesName] = sesButton;



	window.aceSessions[sesName] = ace.createEditSession( text , "ace/mode/" + syntax );

	// При изменениях текста проверять и выводить состояние:
	window.aceSessions[sesName].getDocument().on( "change" , function () {
		documentStatus();
	} );

	sesButton.session = window.aceSessions[sesName];

	//aceSaveButton.sessionButton = sesButton;

	return sesButton;
};

var aceAddNewSessionButton = function () {
	var aceSessionsDiv = document.getElementById( "aceSessionsDiv" );

	//aceSessionsDiv.appendChild( document.createTextNode( " | " ) );

	var newSesButton = document.createElement( "span" );
	newSesButton.innerHTML = "+";
	newSesButton.title = "Добавить в редактор вкладку для нового документа";
	newSesButton.className = "ace-button";
	newSesButton.onclick = aceCreateNewSession;

	aceSessionsDiv.appendChild( newSesButton );
};

var aceCreateNewSession = function ( sesName , text , lang ) {
	var aceSessionsDiv = document.getElementById( "aceSessionsDiv" );

	var button = aceSessionsDiv.lastElementChild;

	if ( !sesName || typeof sesName  !== 'string' || window.aceSessions[sesName] )
		do
			{
				sesName = window.prompt( "Название нового документа" );

				if ( !sesName )
					return false;
			}
		while ( window.aceSessions[sesName] );

	button.innerHTML = sesName;
	button.startText = text ? text : "";
	button.className += " ace-session";
	button.onclick = aceLoadSession;

	aceAddNewSessionButton();


	window.aceSessions[sesName] = ace.createEditSession( "" , "ace/mode/html" );

	// При изменениях текста проверять и выводить состояние:
	window.aceSessions[sesName].getDocument().on( "change" , function () {
		documentStatus();
	} );

	button.session = window.aceSessions[sesName];

	aceLoadSession.call( button );

	if ( lang )
		editor.getSession().setMode( "ace/mode/" + lang );

	if ( text )
		editor.setValue( text );

	return true;
};

// Открыть сессию:
var aceLoadSession = function ( sesName ) {
	if ( hasClass( this , "ace-selected" ) )
		return false;

	if ( this.session )
		session = this.session;
	else if ( sesName && window.aceSessions[sesName] )
		var session = window.aceSessions[sesName];
	else return false;


	var sesButtons = document.getElementById( "aceSessionsDiv" ).getElementsByTagName( "span" );

	for ( var i = 0 ; i < sesButtons.length ; i++ )
		{
			removeClass( sesButtons[i] , "ace-selected" );
		}

	addClass( this , "ace-selected" );


	var buttonsIDs = [
		"aceSaveButton" ,
		"aceTipografButton" ,
		"aceSendTextToYandexSpeller"
	];


	for ( var b = 0 ; b < buttonsIDs.length ; b++ )
		{
			var button = document.getElementById( buttonsIDs[b] );

			if ( button )
				{
					button.startText = this.startText;
					button.session = window.aceSessions[sesName];
					button.sessionButton = this;

					if ( buttonsIDs[b] == "aceSaveButton" && this.text )
						button.text = this.text;

					// Если текст ещё не был сохранён, то указать пустое значение:
					else if ( buttonsIDs[b] == "aceSaveButton" )
						button.text = "";

					else if ( buttonsIDs[b] == "aceForTipographButton" && this.tipograph )
						button.text = this.tipograph;

					else if ( buttonsIDs[b] == "aceSendTextToYandexSpeller" && this.spelled )
						button.text = this.spelled;
				}
		}


	editor.setSession( session );


	if ( window.documentStatus )
		documentStatus();

	return true;
};



// Показать, скрыть непечатные символы:
function wrapUnWrapLongLines () {
	editor.getSession().setUseWrapMode( editor.getSession().getUseWrapMode() ? false : true );

	saveSettings();

	editor.focus();

	return false;
}


// Показать, скрыть непечатные символы:
function showHideUnprinted () {
	editor.setShowInvisibles( editor.getShowInvisibles() ? false : true );

	saveSettings();

	editor.focus();

	return false;
}



// Только для чтения - установить, снять:
function setReadOnly () {
	// Установить/снять режим только чтения:
	editor.setReadOnly( editor.getReadOnly() ? false : true );

	var count = '';

	var mainDiv = document.getElementById( 'aceEditorDiv' );


	// Кнопки редактирования:
	if ( editor.getReadOnly() )
		{
			addClass( mainDiv , "ace_read_only" );
//				addClass(aceEditorMacrosButtons,"false");
//				addClass(aceEditorHelpButtons,"false");
		}
	else
		{
			removeClass( mainDiv , "ace_read_only" );
//				removeClass(aceEditorHelpButtons,"false");
//				removeClass(aceEditorMacrosButtons,"false");

			editor.focus();
		}

	aceEditorsOnScreen();

	saveSettings();

	return false;
}



/**
 * @description Проверят, документ только read only
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 01.07.14 0:17
 *
 * */
var checkReadOnly = function () {
	if ( editor.getReadOnly() )
		{
			alert( "Документ только для чтения" );
			return true;
		}

	editor.focus();

	return false;
};



/**
 * @description Обернуть в строчный HTML тег выделенный фрагмент текста
 *
 * @param {string} [tag] строка с названием тега в который будет обёрнут выделенный текст, строчные теги - http://www.w3schools.com/tags/ref_byfunc.asp
 *
 * @author <a href="mailto:timofey@1september.ru">Timofey - 1september</a>
 * @author <a href="mailto:T-Solovechik@ya.ru">Timofey</a>
 *
 * @version 01.07.14 1:12
 *
 * */
function addLineTag ( tag ) {
	// Проверка режима только для чтения:
	if ( checkReadOnly() )
		return false;

	var selections = editor.getSelection().getAllRanges()
//			,selections = editor.getSession().getSelectionMarkers()
		, selectedTex
		, text
		, startTags = []
		, attributes
		, obj;

	// Если НЕ указано название тега, то предложить окно для ввода названия:
	if ( !arguments.length || !tag.trim() )
		{
			// Запросить url
			tag = window.prompt(
				"Укажите название строчного HTML тега "
				+ "\n"
				+ "http://www.w3schools.com/tags/ref_byfunc.asp"
				+ "\n"
				+ "address cite code del kbd mark meter pre progress span s(strike) sub sup wbr" );

			if ( !tag )
				return false;

			tag = tag.trim();
		}

	var tagEnd = "</" + tag + ">";


	/**
	 * Отправить запрос на указание значения
	 *
	 * @param {string} text
	 *
	 * @param {string|false} [value]
	 */
	function promptToSetValue ( text , value ) {
		var prom = window.prompt( text , value );

		if ( prom )
			return prom;

		return false;
	}

	// Проверка, является ли значение числом, если нет, ещё раз показать окно ввода значения:
	function isNumber ( value ) {
		if ( isNaN( Number( value ) ) )
			{
				return promptToSetValue( "Укажите значение цифрами" );
			}

		return Number( value );
	}

	/**
	 * Перебор всех выделенных фрагментов
	 */
	for ( var s = 0 ; s < selections.length ; s++ )
		{
			startTags[s] = "<" + tag + ">";

			selectedTex = editor.getSession().getDocument().getTextRange( selections[s] );

			// Если добавление ссылки:
			if ( tag == "a" )
				{
					var href = "http://";

					if ( selectedTex )
						href = selectedTex;

					// Запросить url
					href = promptToSetValue( "Укажите адрес" , href );

					if ( !href )
						return false;

					href = ' href="' + href + '"';

					startTags[s] = "<" + tag + href + ">";
				}

			// Если добавление meter:
			if ( tag == "meter" )
				{
					attributes = {
						value : "Укажите значение цифрами дробное (0.5) или от максимального" , min : "Укажите нижнее значение цифрами" , max : "Укажите максимальное значение цифрами"
					};

					for ( obj in attributes )
						{
							if ( !attributes.hasOwnProperty( obj ) )
								continue;

							attributes[obj] = isNumber( window.prompt( attributes[obj] ) );
						}

					// Переписать открывающий тег:
					startTags[s] = "<" + tag + attributes.min + attributes.max + attributes.value + ">";
				}

			// Если добавление progress:
			if ( tag == "progress" )
				{
					attributes = {
						max : "Укажите максимальное значение цифрами" , value : "Укажите значение цифрами от максимального"
					};

					for ( obj in attributes )
						{
							if ( !attributes.hasOwnProperty( obj ) )
								continue;

							attributes[obj] = isNumber( window.prompt( attributes[obj] ) );
						}

					// Переписать открывающий тег:
					startTags[s] = "<" + tag + attributes.max + attributes.value + ">";
				}

			// Если добавление wbr - убрать закрывающий тег:
			if ( tag == "wbr" )
				tagEnd = "";

			text = startTags[s] + selectedTex + tagEnd;


			// Заменить текст:
			editor.session.replace( selections[s] , text );

		}

	editor.focus();

	return false;
}




var addBlockTag = function ( tag ) {
	// Проверка режима только для чтения:
	if ( checkReadOnly() )
		return false;

	var
		tab = editor.getSession().getTabString()
		, tabSize = editor.getSession().getTabSize()
	// На сколько строк выше поднять курсор:
		, rows = null
		, leadingTabs = ''

		, selections = editor.getSelection().getAllRanges()
//			,selections = editor.getSession().getSelectionMarkers()
		, selectedTex
		, selectionPos
		, text
		, insert = false
		, startTags = [];

	var replaceBeforeSet = function ( text ) {
		text = text.replace( "\r\n" , "\n" );
		text = text.replace( "\r" , "\n" );

		text = text.replace( /\v/g , "\t" );
		var replRegExp = new RegExp( "( {1," + tabSize + "}(?![^\t])|\t+ {1," + tabSize + "}| {2,})" , "mg" );
		text = text.replace( replRegExp , tab );

		return text;
	};

	// Если НЕ указано название тега, то предложить окно для ввода названия:
	if ( !arguments.length || !tag.trim() )
		{
			// Запросить url
			tag = window.prompt(
				"Укажите название строчного HTML тега "
				+ "\n"
				+ "http://www.w3schools.com/tags/ref_byfunc.asp"
				+ "\n"
				+ "address cite code del kbd mark meter pre progress span s(strike) sub sup wbr" );

			if ( !tag )
				return false;

			tag = tag.trim();
		}

	/**
	 * Перебор всех выделенных фрагментов
	 */
	for ( var s = selections.length - 1 ; s >= 0 ; s-- )
		{
			selectedTex = editor.getSession().getDocument().getTextRange( selections[s] );

			startTags[s] = "<" + (
				tag == "li" ? (
				tag + ' style="margin-top: 1em;"'
				) : tag
			) + ">";

			var tagEnd = "</" + tag + ">";

			// Если ничего не выделено:
			if ( selectedTex == "" )
				{
					selectionPos = selections[s].start;
					selectionPos.column = 0;

					// Выделить все строку и сохранить для замены:

					text = editor.getSession().getDocument().getLine( selections[s].start.row )
					//text = editor.getSession().getSelection().selectLine(selections[s].start.row);

					console.log( 'text: ' + (
						text
					) );

					// Отступ перед началом теста:
					leadingTabs = text.match( /^\s*/ );

					/**
					 * Убрать в начале строки все отступы
					 */
					text = text.replace( leadingTabs , "" );

					// Выбранная строка - тег:
					var textTags = new RegExp( "<(\/?)([^\s]+)[^>]*>" );

					// Проверить и сохранить результат:
					var match = text.match( textTags );

					// Если есть тег и он блочный, то...:
					if ( match && match.length <= 3 && ["div" , "p" , "table" , "tr" , "td" , "ol" , "ul" , "li"].indexOf( match[2] ) != -1 )
						{
							/**
							 * Если закрывающий тег, то блок ниже
							 */
							if ( match[1] == "/" )
								{
									selectionPos.row = selectionPos.row + 2;
									editor.getSession().getDocument().insertNewLine( selectionPos );
									editor.getSession().getDocument().insertNewLine( selectionPos );
								}
							else
								{
									editor.getSession().getDocument().insertNewLine( selectionPos );
									editor.getSession().getDocument().insertNewLine( selectionPos );
								}

							text = "";

							insert = true;
						}

					/**
					 * Если добавляется блок списка
					 */
					if ( ["ol" , "ul"].indexOf( tag ) !== -1 )
						{
							text = ""
								   + leadingTabs + startTags[s]
								   + "\n"
								   + "\n" + leadingTabs + tab + '<li style="margin-top: 1em;">'
								   + "\n" + leadingTabs + tab + tab + text
								   + "\n" + leadingTabs + tab + "</li>"
								   + "\n"
								   + "\n" + leadingTabs + tagEnd;
							rows = 4;
						}
					/**
					 * Другие блочные тег
					 */
					else
						{
							text = ""
								   + leadingTabs + startTags[s]
								   + "\n" + leadingTabs + tab + text
								   + "\n" + leadingTabs + tagEnd;
							rows = 2;
						}


					/**
					 * Добавить текст
					 */
					if ( insert )
						editor.getSession().getDocument().insert( selectionPos , text );
					else selections[s].end.column = editor.getSession().getDocument().getLine( selections[s].start.row ).length;

				}
			// Если есть выделенный фрагмент:
			else
				{
					text = replaceBeforeSet( selectedTex );

					// Если выбрана пустая строка, то заменить её на отступ:
					if ( /^\s$/.test( text ) )
						{
							text = tab;
						}

					// Строка начала выделения:
					var start = selections[s].start.row;
					// Строка завершения выделения:
					var end = selections[s].end.row;

					/**
					 * Поробовать в фрагменте найти начальные отступы
					 */
					while ( start <= end )
						{
							// Взять всю строку:
							var lineText = editor.getSession().getDocument().getLine( start );

							/**
							 * Если найдены предстрочные отступы
							 */
							if ( leadingTabs = lineText.match( /^\s+/mg ) )
								break;

							else leadingTabs = [""];
							start++;
						}



					// Убрать переносы строк из отступа строки:
					leadingTabs = leadingTabs[0].replace( "\n" , "" );


					if ( ["ol" , "ul"].indexOf( tag ) !== -1 )
						{
							// Без пустых строк:
							text = text.replace( /\n\n/gm , "\n" );
							// Убрать отступы в начале:
							text = text.replace( /^\s+/gm , "" );

							// Каждую строку в list:
							text = text.replace( /(^.+$)/gm ,
								"\n" + '<li style="margin-top: 1em;">'
								+ "\n" + tab + "$1"
								+ "\n" + "</li>"
								+ "\n" );
							// Добавить leadingTabs не пустым строкам:
							text = text.replace( /^(?=.+$)/gm , leadingTabs + tab );

							// Собрать весть текст:
							text = "\n"
								   + leadingTabs + startTags[s]
								   + "\n"
								   + text
								   + "\n"
								   + leadingTabs + tagEnd;

							rows = 0;
						}
					else
						{
							/** К каждой новой строке добавить <br> */
							if ( ["p"].indexOf( tag ) !== -1 )
								{
									/**
									 * Добавить отступы br
									 *
									 * Если br, то заменить
									 */
									text = text.replace( /(\s*(<br ?\/?>)?(\s*))?\n(?!\s*(<br ?\/?>))/mig , "\n" + leadingTabs + "<br>" + "\n$3" );
								}

							text = leadingTabs + text;

							text = "\n"
								   + leadingTabs + startTags[s]
								   + "\n" + text.replace( /^/mg , tab )
								   + "\n" + leadingTabs + tagEnd;

							rows = 1;
						}


				}

			// Заменить текст:
			if ( !insert )
				editor.getSession().replace( selections[s] , text );

			var row = selections[s].start.row + rows;

			// Переместить курсор к концу строки:
			editor.gotoLine( row , editor.getSession().getDocumentLastRowColumn( row ) , true );
		}

	// Вернуться к редактору:
	editor.focus();

	return false;
};



function addInfoMarks () {
	editor.getSession().setAnnotations(
		[
			{
				row    : 30 , // must be 0 based
				column : 15 ,  // must be 0 based
				text   : "Текст аннотации к изданию отсуствует" ,  // text to show in tooltip
				type   : "warning"
			} ,
			{
				row    : 102 , // must be 0 based
				column : 15 ,  // must be 0 based
				text   : "Текст аннотации к изданию отсуствует" ,  // text to show in tooltip
				type   : "warning"
			}
		]
	)
}
