define('ace/ext/statusbar', ['require', 'exports', 'module' , 'ace/lib/dom', 'ace/lib/lang'], function(require, exports, module) {
var dom = require("ace/lib/dom");
var lang = require("ace/lib/lang");

var StatusBar = function(editor, parentNode) {
    this.element = dom.createElement("div");
    this.element.className = "ace_status-indicator";
    this.element.style.cssText = "display: inline-block;";
    parentNode.appendChild(this.element);

    var statusUpdate = lang.delayedCall(function(){
        this.updateStatus(editor)
    }.bind(this));

    editor.on("changeStatus", function() {
        statusUpdate.schedule(450);
    });

    editor.on("changeSelection", function() {
        statusUpdate.schedule(450);
    });

    editor.getSession().addEventListener("changeMode", function() {
        statusUpdate.schedule(450);
		//console.log("status");
    });
};

(function(){
    this.updateStatus = function(editor) {
        var status = [];
        function add(str, separator) {
            str && status.push(str, separator || " | ");
        }

	// VIM status:
        if (editor.$vimModeHandler){
            add(editor.$vimModeHandler.getStatusText());}
	// REC:
        else if (editor.commands.recording)
            add("REC");

//		var theme = editor.getTheme().split('/').pop();
//		add(theme);

		var text = " | ";

		// cursor position (selection):
        var c = editor.selection.lead;
		text += c.row + ":" + c.column;

//        if (!editor.selection.isEmpty()) {
				var r = editor.getSelectionRange();

				var
					selections = editor.getSession().getSelectionMarkers()
//					,selections = editor.getSelection().getAllRanges()
					,length = 0;

				for ( var s = 0 ; s < selections.length ; s++ )
					{
						length += editor.getSession().getDocument().getTextRange(selections[s]).length;
					}

				if ( !length )
					length = editor.getSelectedText().length;

				text += " (" + (r.end.row - r.start.row) + ":" + length + ")";
//			}
        add(text);

	// Mode - syntax's - highlighting:
		var mode = editor.getOption("mode").split("/").pop();
		add(mode);

        status.pop();
        this.element.textContent = status.join("");
    };
}).call(StatusBar.prototype);

exports.StatusBar = StatusBar;

});
		(function() {
				window.require(["ace/ext/statusbar"], function() {});
			})();
