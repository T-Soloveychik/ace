/**
 * Created by PhpStorm
 *
 * @version 2014-08-1 5:26
 */


if ( window.acePassTextThroughTypograph )
	{
		var aceDivFor = document.getElementById( "aceForTipographButton" );

		/**
		 * <span class="ace-button" id="aceTipografButton" title="Alt-t – Пропустить текст через Типограф"
		 *   onclick="aceGetAndReplaceTextByTypographedText(event);">Типограф</span>
		 */

		aceDivFor.appendChild( document.createTextNode( " | " ) );

		var aceTipografButton = document.createElement( "span" );
		aceTipografButton.id = "aceTipografButton";
		aceTipografButton.className = "ace-button ace_writers";
		aceTipografButton.title = "Alt-t – Пропустить текст через Типограф";
		aceTipografButton.innerHTML = "Типограф";
		aceTipografButton.onclick = aceGetAndReplaceTextByTypographedText;

//		aceTipografButton.status;
//		aceTipografButton.text = editor.getValue();
		aceTipografButton.startText = editor.getValue();

		aceDivFor.appendChild( aceTipografButton );

	}

/**
 * ОтТипографить
 *
 * @author Timofey <a href="mailto:T-Soloveychik@ya.ru">T-Soloveychik@ya.ru</a>
 *
 * @param {object} event
 *
 * @version 2014-08-1 5:26
 *
 */
function aceGetAndReplaceTextByTypographedText ( event ) {
//		console.log(event);
	// Проверка режима только для чтения:
	if ( checkReadOnly() )
		return false;


	window.aceTipographButton = event.target;

	if ( window.acePassTextThroughTypograph )
		{

			if ( editor.getSelectedText() )
				{
					var selections = editor.getSelection().getAllRanges();

					aceTipographButton.selections = selections;

					for ( var selectedTex , s = 0 ; s < selections.length ; s++ )
						{
							selectedTex = editor.getSession().getDocument().getTextRange( selections[s] );

							if ( selectedTex.trim() == "" )
								continue;

							aceTipographButton.selection = s;

							acePassTextThroughTypograph( selectedTex , aceTipographButton , selections[s] );

							break;
						}
				}
			else
				{

					// Если JSON, то alert с завершением:
					var syntax = editor.getOption( "mode" ).split( "/" ).pop();

					if ( syntax == "json" )
						return alert( "Это JSON – его не нужно Типографировать" );

					else if ( syntax == "sql" )
						return alert( "Это sql – его не нужно Типографировать" );

					else if ( syntax == "mysql" )
						return alert( "Это mysql – его не нужно Типографировать" );

					acePassTextThroughTypograph( editor.getValue() , aceTipographButton );
				}

		}
	else
		alert( "Не заданы параметры для Типографирования!" );


	return editor.focus();

}

/**
 *
 */
var aceGetReturnedTypographedValue = function ( text , rangeOrStatusText , response ) {

	if ( text )
		{
			// Вывести текст в редакторе:
			if ( rangeOrStatusText )
				{
					editor.session.replace( rangeOrStatusText , text );

					var selections = aceTipographButton.selections;
					var s = aceTipographButton.selection;

					if ( ++s == selections.length )
						{
							aceTipographButton.selections = false;

							return true;
						}

					for ( var selectedTex; s < selections.length ; s++ )
						{
							selectedTex = editor.getSession().getDocument().getTextRange( selections[s] );

							if ( selectedTex.trim() == "" )
								continue;

							acePassTextThroughTypograph( selectedTex , aceTipographButton , selections[s] );

							break;
						}

					return true;
				}

			editor.setValue( text );

			aceTipographButton.text = editor.getValue();
			aceTipographButton.status = true;

			/**
			 * Запомнить на кнопке сессии, что текст оттипографирован
			 */
			aceTipographButton.sessionButton.tipograph = editor.getValue();

			// Перепроверить статус ОтТипографированности:
			documentStatus( "ПроТипографированно" );
		}
	else
		{
			documentStatus( rangeOrStatusText );

			alert( "Не получилось Типографировать!"
				   + "\n" + "При обработке ответа были ошибка:"
				   + "\n" + response
			);
		}


	return editor.focus();

};

