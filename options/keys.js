
//// Сохранить
//editor.commands.addCommand({
//	name: "save",
//	bindKey: {win: "Ctrl-s", mac: "Command-s"},
//	exec: function(editor) {
//		aceSaveButton.click()
//	}
//});
//editor.commands.addCommand({
//	name: "execute",
//	bindKey: "ctrl+enter",
//	exec: function(editor) {
//		try
//			{
//				var r = window.eval(editor.getCopyText() || editor.getValue());
//			}
//		catch(e) { r = e; }
//
//		editor.cmdLine.setValue(r + "");
//	}
//});
//cmdLine.commands.bindKeys({
//	"Shift-Return|Ctrl-Return|Alt-Return": function(cmdLine) { cmdLine.insert("\n"); },
//	"Esc|Shift-Esc": function(cmdLine){ cmdLine.editor.focus(); },
//	"Return": function(cmdLine){
//		var command = cmdLine.getValue().split(/\s+/);
//		var editor = cmdLine.editor;
//		editor.commands.exec(command[0], editor, command[1]);
//		editor.focus();
//	}
//});

//cmdLine.commands.removeCommands(["find", "gotoline", "findall", "replace", "replaceall"]);

// Типограф
editor.commands.addCommand({
	name: "typograph",
	bindKey: {win: "Alt-t", mac: "Alt-t"},
	exec: function(editor) {
		aceTipografButton.click()
	}
});

// Menu
editor.commands.addCommand({
	name: "showMenu",
	bindKey: {win: "Ctrl-Alt-m", mac: "Command-Alt-m"},
	exec: function(editor) {
		editor.showSettingsMenu()
	}
});

// add command to lazy-load keybinding_menu extension
editor.commands.addCommand({
	name: "showKeyboardShortcuts",
	bindKey: {win: "Ctrl-Alt-h", mac: "Command-Alt-h"},
	exec: function(editor) {
		editor.showKeyboardShortcuts()
	}
});

// Ctrl+Alt+P – скрыть/отобразить непечатные
editor.commands.addCommand({
	name: "showHideInvisibles",
	bindKey: {win: "Ctrl-Alt-p", mac: "Command-Alt-p"},
	exec: function(editor) {
		showHideUnprinted();
	}
});

// Alt-a - link
editor.commands.addCommand({
	name: "wrapTextByLink",
	bindKey: {win: "Alt-a", mac: "Alt-a"},
	exec: function(editor) {
		addLineTag('a');
	}
});

// Alt-b - boild
editor.commands.addCommand({
	name: "wrapTextByBoild",
	bindKey: {win: "Alt-b", mac: "Alt-b"},
	exec: function(editor) {
		addLineTag('b');
		return false;
	}
});

// Alt-b - italic
editor.commands.addCommand({
	name: "wrapTextByItalic",
	bindKey: {win: "Alt-i", mac: "Alt-i"},
	exec: function(editor) {
		addLineTag('i');
	}
});

// Alt-t - inline Tag
editor.commands.addCommand({
	name: "wrapOrAddToTextByInLineTag",
	bindKey: {win: "Alt-t", mac: "Alt-t"},
	exec: function(editor) {
		addLineTag();
	}
});

// Alt-u - underline
editor.commands.addCommand({
	name: "wrapTextByUnderline",
	bindKey: {win: "Alt-u", mac: "Alt-u"},
	exec: function(editor) {
		addLineTag('u');
	}
});

// Alt-p - paragraph
editor.commands.addCommand({
	name: "wrapTextByParagraph",
	bindKey: {win: "Alt-p", mac: "Alt-p"},
	exec: function(editor) {
		addBlockTag('p');
	}
});

// Alt-div - Division
editor.commands.addCommand({
	name: "wrapTextByDivision",
	bindKey: {win: "Alt-d", mac: "Alt-d"},
	exec: function(editor) {
		addBlockTag('div');
	}
});

// Alt-Shift-u - UnOrderedList
editor.commands.addCommand({
	name: "wrapTextByUnOrderedList",
	bindKey: {win: "Alt-Shift-u", mac: "Alt-Shift-u"},
	exec: function(editor) {
		addBlockTag('ul');
	}
});

// Alt-Shift-o - OrderedList
editor.commands.addCommand({
	name: "wrapTextByOrderedList",
	bindKey: {win: "Alt-Shift-o", mac: "Alt-Shift-o"},
	exec: function(editor) {
		addBlockTag('ol');
	}
});

// Alt-l - List - li
editor.commands.addCommand({
	name: "wrapTextByList",
	bindKey: {win: "Alt-l", mac: "Alt-l"},
	exec: function(editor) {
		addBlockTag('li');
	}
});

// Alt-r - ReadOnly
editor.commands.addCommand({
	name: "ReadOnly",
	bindKey: {win: "Alt-r", mac: "Alt-r"},
	exec: function(editor) {
		setReadOnly();
		// readOnlyIMG.className = editor.getReadOnly();
	}
});

// Esc - fullWindowMode
editor.commands.addCommand({
	name: "fullWindowMode",
	bindKey: {win: "Esc", mac: "Esc"},
	exec: function(editor) {
		fullWindowMode();
	}
});
