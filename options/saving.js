/**
 * @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
 *
 * @version 2014-08-12 14:45
 *
 * @TODO: On EXIT! if not saved - save text local.storage
 */


var aceDivFor = document.getElementById("aceForSaveButton");

/**
 * <span class="ace-button" id="aceSaveButton" title="Ctrl+S (Commans+S) – Сохранить текст"
 * onclick="aceStartTextSavingProcess(event);">Сохранить</span>
 */

aceDivFor.appendChild(document.createTextNode(" | "));

var aceSaveButton = document.createElement("span");
aceSaveButton.id = "aceSaveButton";
aceSaveButton.className = "ace-button";
aceSaveButton.title = "Ctrl+S (Commans+S) – Сохранить текст";
aceSaveButton.innerHTML = "Сохранить";
aceSaveButton.onclick = function(event) { aceStartTextSavingProcess(event) };

if ( window.aceSessionButtons )
	{
		for ( var sesButton in window.aceSessionButtons )
			{

			}
		aceSaveButton.sessionButton = window.aceSessionButtons[sesButton];
	}

//aceSaveButton.status = true;
// aceSaveButton.text = editor.getValue();
aceSaveButton.startText = editor.getValue();

aceDivFor.appendChild(aceSaveButton);


/**
 * Сохранить текст
 *
 * @author Timofey <a href="mailto:T-Soloveychik@ya.ru">T-Soloveychik@ya.ru</a>
 *
 * @param {event} event
 *
 * @version 2014-08-1 5:26
 */
var aceStartTextSavingProcess = function ( event )
	{
		event.preventDefault();
//		// Проверка режима только для чтения:
//			if ( checkReadOnly() )
//				return false;

		// Для сбора всех замечаний:
		var messages = [];
		// Для сбора текстов:
		var text = "";
		// Строка объединения:
		var joinString = "\n    ";


		// Проверить, были ли изменения в тексте:
		if ( aceSaveButton.text == editor.getValue() )
			{
				// Текст для вывода сообщения:
				text = [
					"В тексте нет никаких изменений"
				].join( joinString );
				messages.push( text );
			}


		// Проверить, есть ли предупреждения:
		var Annotations = editor.getSession().getAnnotations();
		if ( Annotations.length )
			{
				// Текст для вывода сообщения:
				text = [
					"В тексте есть места, которые отмечены как проблемные"
					, "Alt+e – для перехода к замечанию"
				].join( joinString );
				messages.push( text );
			}


		// Лишние пробелы:
		var findsCount = selectUselessSpaces();
		// Если есть найденные строки, то:
		if ( findsCount )
			{
				// Текст для вывода сообщения:
				text = [
					"В тексте найдены лишние пробельные символы"
					, "Нажмите на «Выделить лишние пробелы»"
				].join( joinString );
				messages.push( text );
			}


		// Если установлено типографиврование и НЕ ОтТипографирован текст:
		if ( aceTipografButton && !aceTipografButton.status )
			{
				// Текст для вывода сообщения:
				text = [
					"Последние изменения в тексте не ОтТипографированны"
					, "Alt+t – для обработки текста Типографом"
				].join( joinString );
				messages.push( text );
			}


		// Пустая переменная для подготовки текста:
		text = "";

		// Собрать все сообщения:
		for ( var mesgNum = 0 ; mesgNum < messages.length ; mesgNum++ )
			{
				text += mesgNum + 1 + ". " + messages[mesgNum];
				text += "\n";
				text += "––––––––––––––––––––––––––––";
				text += "\n";
			}

		// Сообщение о продолжении сохранения:
		text += [
			"Все изменения в тексте можно отменить:"
			, "Ctrl+z (Command+z) – отменить изменения"
			, "Ctrl+Shift+z (Command+Shift+z) – вернуть изменения"
		].join( "\n" );

		text += "\n";
		text += "\n";

		// Сообщение о продолжении сохранения:
		text += [
			"Отмена – для отмены сохранения"
			, "OK – для сохранения изменений"
		].join( "\n" );


		// Если подтверждается продолжение выполнения:
		if ( window.confirm( text ) )
			{
				/**
				 * Если прописана функция сохранения текста по передать в неё текст
				 */
				if ( window.aceRedactorSaveText )
					{
						// Сохранить установки редактора:
						saveSettings();

						window.aceRedactorSaveText( editor.getValue() , aceSaveButton );
					}
				else alert( "Не заданы параметры сохранения!" );
			}

		// Вернуться к редактору:
		editor.focus();

	};


/**
 * Функция проверки сохранённого текста
 *
 * @function aceGetReturnedTextSavingStats
 *
 * @param {string} text - сохранённый текст
 * @param {string} [statusText]
 * @param {string} [response]
 *
 */
var aceGetReturnedTextSavingStats = function ( text , statusText , response )
	{
		/**
		 * Если переданный текст равен тому, что в редакторе
		 */
		if ( text == editor.getValue() )
			{
				aceSaveButton.status = true;
				aceSaveButton.text = text;

				/**
				 * Запомнить на кнопке сессии, что текст оттипографирован
				 */
				aceSaveButton.sessionButton.text = text;

				documentStatus( "Текст сохранён" );
			}
		else
			{
				var textToEditor = ""
									   + "При сохранении произошла ошибка, возвращённый текст не равен тому, что в редакторе!"
									   + "\n"
									   + "\n"
									   + "Для отмены изменений нажмите Ctrl-Z (Alt-Z)"
									   + "\n"
									   + "\n"
									   + text
									   + "\n"
									   + "\n"
									   + statusText
									   + "\n"
									   + "\n"
						+ response
					;

				editor.setValue( textToEditor );

				documentStatus( "Ошибка" );
			}

		// Вернуться к редактору:
		editor.focus();
	};



// При выходе:
//window.onunload = function ()
//	{
//		var date = new Date();
//
//		var title = "ace-" + location.search;
//		var time = title + "-time";
//
//		var text = editor.getValue();
//		text = text.replace( /"/gim , "\"" );
//		text = text.replace( /\n/gim , "\\n" );
//
//		localStorage.setItem( time , date.getTime() );
//		localStorage.setItem( title , text );
//	};
