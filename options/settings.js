/**
 * Created by Timofey on 2014-06-26 10:25
 *
 */


// Элемент для выведения списка:
var themesList = document.getElementById( "themeEl" );
var themesNames = {
	"Сохранённая тема" : [
	] , "Светлые"      : [
		"chrome"
		, "cloud9_day"
		, "clouds"
		, "crimson_editor"
		, "dawn"
		, "dreamweaver"
		, "eclipse"
		, "github"
		, "katzenmilch"
		, "kuroir"
		, "solarized_light"
		, "textmate"
		, "tomorrow"
		, "xcode"
	] , "Тёмные"       : [
		"ambiance" // Новая с шумом, красиво
		, "cloud9_night"
		, "cloud9_night_low_color"
		, "clouds_midnight"
		, "cobalt"
		, "idle_fingers"
		, "kr_theme"
		, "merbivore"
		, "merbivore_soft"
		, "mono_industrial"
		, "monokai"
		, "pastel_on_dark"
		, "solarized_dark"
		, "terminal"
		, "tomorrow_night"
		, "tomorrow_night_blue"
		, "tomorrow_night_bright"
		, "tomorrow_night_eighties"
		, "twilight"
		, "vibrant_ink"
	] , "Другие"       : [
		, "kr"
	]
};


function aceBuildThemeSelector()
	{
		// Установленная тема:
		var nowTheme = editor.getTheme();

//		console.log('nowTheme: ' + (nowTheme));

		if ( nowTheme )
			nowTheme = editor.getTheme().split( '/' ).pop();
		else
			{
				nowTheme = "chrome";
				editor.setTheme( "ace/theme/" + nowTheme );
			}

//		console.log('nowTheme: ' + (nowTheme));

		// Перебор массива:
		for ( var thmgrp in themesNames )
			{
				if ( !themesNames.hasOwnProperty( thmgrp ) )
					continue;

				// Разделитель группы:
				var grp = document.createElement( 'optgroup' );
				grp.setAttribute( "label" , thmgrp );
				themesList.appendChild( grp );

				// Перебор тем в группе:
				for ( var thm in themesNames[thmgrp] )
					{
						if ( !themesNames[thmgrp].hasOwnProperty( thm ) )
							continue;

						var opt = document.createElement( 'option' );
						opt.setAttribute( "value" , themesNames[thmgrp][thm] );
						opt.innerHTML = themesNames[thmgrp][thm];

						// Если выбрана данная тема:
						if ( themesNames[thmgrp][thm] == nowTheme )
							{
								opt.setAttribute( "selected" , "selected" );
								themesList.firstElementChild.appendChild( opt );
							}
						else grp.appendChild( opt );
					}
			}

		// Сменить тему:
		var changeTheme = function ()
			{
				if ( !this.value )
					return;

				editor.setTheme( "ace/theme/" + this.value );

				// Сохранить настройки:
				saveSettings();
			};

		// Установить тему из списка:
		themesList.onchange = changeTheme;
		themesList.onkeyup = changeTheme;
		themesList.onmousemove = changeTheme;

	}

// Сохранение всех установок редактора:
	var saveSettings = function()
		{
			var settings = editor.getOptions();

			var toString = JSON.stringify(settings);

			// console.log(toString);
			var expires = 60 * 60 * 24 * 30;
			setCookie("settings",toString,{"path":"/","secure":true,"expires":expires});
		};

// Восстановить установки редактора:
	var restoreSettings = function()
		{
			var generalSettings = {
				animatedScroll: true
				// enable autocompletion and snippets
				,enableBasicAutocompletion: true
				,enableSnippets: true
				,enableLiveAutocompletion: true
				// Переносить по словам:
				,wrapBehavioursEnabled: true
				// EOL - \n:
				,newLineMode: "unix"
			};

		// Если режим только для чтения, скрыть кнопки редактирования:
			aceEditorHelpButtons.className = editor.getReadOnly() ? false : "";
		// Cookie->JSON->Object:
			var cooksSettings = eval( "(" + getCookie("settings") + ")");

			// console.log(cooksSettings);
			if ( cooksSettings )
				editor.setOptions(cooksSettings);
		// Если нет Cookie->JSON->Object:
			else
				{
				// Поумолчанные настройки:
					editor.setOptions({
						// Syntax:
							mode: "ace/mode/html"
						// Тема подсветки:
							,theme: "ace/theme/chrome"
						// Плавная прокрутка
							,animatedScroll: true
						// enable autocompletion and snippets
							,enableBasicAutocompletion: true
							,enableSnippets: true
							,enableLiveAutocompletion: true
						// Отображать непечатные символы:
							,showInvisibles: true
						// Убрать авто-размер:
							,autoScrollEditorIntoView: false
							,minLines: false
							,maxLines: false
						// Переносить по словам:
							,wrapBehavioursEnabled: true
						// TAB как \t а не пробелы:
							,useSoftTabs: false
						// Переносить длинные строки:
							,useWrapMode: false
						});

				}


			editor.setOptions(generalSettings);


			/**
			 *  Если переданы дополнительные параметры, то установить их
			 */
			if ( typeof aceEditorOptionsToSet == "object" )
				{
					editor.setOptions(aceEditorOptionsToSet);
				}



			aceBuildThemeSelector();



			/**
			 * Если только для чтения
			 */
			var mainDiv = document.getElementById("aceEditorDiv");

			if ( editor.getReadOnly() )
				addClass(mainDiv,"ace_read_only");


//		// Auto-load syntax's
//			(function () {
//				var modelist = ace.require("ace/ext/modelist");
//				// the file path could come from an xmlhttp request, a drop event,
//				// or any other scriptable file loading process.
//				// Extensions could consume the modelist and use it to dynamically
//				// set the editor mode. Webmasters could use it in their scripts
//				// for site specific purposes as well.
//				var filePath = "blahblah/weee/some.js";
//				var filePath = "templates/index.tpl";
//				var filePath = "index.html";
//				var mode = modelist.getModeForPath(filePath).mode;
//				console.log(mode);
//				editor.session.setMode(mode);
//			}());
		};

// Основные установки:
	restoreSettings();


// Кнопки редактирования:
	var aceEditorPre = document.getElementById("aceEditorMainWindow");

// Редактор на всё окно:
	var fullWindowMode = function()
		{
			/**
			 * Статусная строка редактора
			 */
			var aceStatusBar = document.getElementById ( "aceStatusBar" );

//			console.log('/ace_editor_full_window/.test(aceEditorPre.parentNode.className): ' + (/ace_editor_full_window/.test(aceEditorPre.parentNode.className)));
		// Вернуть:
			if ( /ace_editor_full_window/.test(aceEditorPre.parentNode.className) )
				{
					document.getElementsByTagName("html")[0].style.overflow = "";
					document.getElementsByTagName("body")[0].style.overflow = "";

					removeClass(aceEditorPre.parentNode,"ace_editor_full_window");
					removeClass(aceEditorHelpButtons,"fixed");
					removeClass(aceStatusBar,"fixed");

					aceEditorsOnScreen();

					aceEditorPre.parentNode.style.height = "";
					aceEditorPre.style.height = "";

					/**
					 * Убрать отступ сверху
					 */
					aceEditorPre.style.top = "";

					if ( editor.wasOptins )
						for ( var o in editor.wasOptins )
							{
								if ( !editor.wasOptins.hasOwnProperty( o ) )
									continue;

								editor.setOption(o, editor.wasOptins[o]);
							}
				}
		// На всё окно:
			else
				{
					editor.wasOptins = {};
					editor.wasHeight = aceEditorPre.style.height;

					editor.wasOptins.autoScrollEditorIntoView = editor.getOption('autoScrollEditorIntoView');
					editor.wasOptins.minLines = editor.getOption('minLines');
					editor.wasOptins.maxLines = editor.getOption('maxLines');


					document.getElementsByTagName("html")[0].style.overflow = "hidden";
					document.getElementsByTagName("body")[0].style.overflow = "hidden";

					aceEditorPre.parentNode.style.height = aceEditorPre.offsetHeight + "px";
					aceEditorPre.style.height = "";



					aceEditorHelpButtons.style.width = "";
					aceStatusBar.style.width = "";

					/**
					 * Отступ сверху по высоте панели вспомогатедьных кнопок
					 */
					aceEditorPre.style.top = aceEditorHelpButtons.offsetHeight + "px";

					// Убрать авто-размер:
					editor.setOptions({
						autoScrollEditorIntoView: false
						,minLines: false
						,maxLines: false
					});

					addClass(aceEditorPre.parentNode,"ace_editor_full_window");
					addClass(aceEditorHelpButtons,"fixed");
					addClass(aceStatusBar,"fixed");
					console.log ( aceEditorHelpButtons.className )
				}

		// Переразмерить:
			editor.resize();
		// Вернуться к редактору:
			editor.focus();
		};
