/**
 * Created by Created by PhpStorm.
 *
 * @file
 *
 * Коротко – пояснение
 *
 * Детальное описание
 *
 * @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
 *
 * @date 2014-06-26 22:22
 */



// Ctrl[+Shift]+s на странице сохраняет текст:
window.onkeypress = function ( event )
	{
		// Если нажат Ctrl:
		if ( event.ctrlKey )
			{
				// + кнопка s или S, то сохранить:
				if ( event.which == 115 || (
					event.shiftKey && event.which == 83
					) )
					{
						event.preventDefault();
						// Сбросить установленное событие:
						aceStartTextSavingProcess( event );

						return false;
					}
				// + кнопка r или R, показать строку поиска:
				if ( event.which == 114 || (
					event.shiftKey && event.which == 82
					) )
					{
						event.preventDefault();
						// Сбросить установленное событие:
						showSearchBox();
					}
				// + кнопка h - страница с hot kyes:
				if ( event.which == 104 )
					{
						window.open( 'https://github.com/ajaxorg/ace/wiki/Default-Keyboard-Shortcuts' );

						// Сбросить установленное событие:
						return false;
					}
				else
					{
					}

			}

		// F1 show help:
		//if ( event.charCode == "112" || event.charCode == "112" )
		//	{
		//		window.open('https://github.com/ajaxorg/ace/wiki/Default-Keyboard-Shortcuts');
		//	}
		/*
		 console.log(event);
		 console.log("event.charCode: " + event.charCode);
		 console.log("event.keyCode: " + event.keyCode);
		 console.log("event.keyIdentifier: " + event.keyIdentifier);
		 console.log("event.key: " + event.key);
		 console.log("event.which: " + event.which);
		 console.log("event.metaKey: " + event.metaKey);
		 return false;
		 */

		return true;
	};

// var editorChanges = function()
// 	{
// 		alert("some");
// 	};

//editor.onChangeWrapMode = editorChanges();
//editor.onChangeWrapLimit = editorChanges();
//editor.session.changeWrapLimit = editorChanges();
// editor.session.changeTabSize = function ( e ){
// 		console.log("changeTabSize")
// 	};
// editor.session.changeMode = editorChanges();
//editor.changeSession = editorChanges();


//// Выделение номера строки:
//	editor.renderer.$gutter.addEventListener("click", function(e) {
//		// console.log( e.target );
//		var gutter = e.target;
//		var gutRow = editor.selection.getCursor()["row"];
//		if ( /marked/.test(gutter.className) )
//			editor.session.removeGutterDecoration(gutRow,"marked");
//		else
//			editor.session.addGutterDecoration(gutRow,"marked");
//	});




// Строка редактирования всегда на виду:
var aceEditorHelpButtons = document.getElementById( "aceEditorHelpButtons" );
aceEditorHelpButtons.startOffsetTop = aceEditorHelpButtons.offsetTop;


/**
 * При прокрутке фиксировать в зоне видимости окна редактора статусную строку и кнопки редактирования
 */
var aceEditorsOnScreen = function ()
	{

		var className = "fixed";

		/**
		 */
		var aceEditorPre = document.getElementById( "aceEditorMainWindow" );


		if ( hasClass( aceEditorPre , className ) )
			return false;


		var editorTop = aceEditorPre.parentNode.parentNode.offsetTop + aceEditorPre.parentNode.offsetTop;

		var editorHeight = aceEditorPre.offsetHeight;

		var editorButtonsMaxY = editorTop + editorHeight;

		var top = aceEditorHelpButtons.parentNode.parentNode.offsetTop + aceEditorHelpButtons.parentNode.offsetTop;

//		console.log( 'aceEditorHelpButtons.className: ' + aceEditorHelpButtons.className );
//		console.log( 'top: ' + top );
//		console.log( 'window.scrollY: ' + window.scrollY );
//		console.log( 'editorTop: ' + (editorTop) );
//		console.log( 'editorHeight: ' + (editorHeight) );
//		console.log( 'editorButtonsMaxY - aceEditorHelpButtons.offsetHeight: ' + (editorButtonsMaxY - aceEditorHelpButtons.offsetHeight) );




		if ( hasClass( aceEditorPre.parentNode , "ace_editor_full_window" ) || (
			top < window.scrollY && window.scrollY <= editorButtonsMaxY - aceEditorHelpButtons.offsetHeight
			) )
			{
				/**
				 * Задать блоку высоту, чтобы не съезжали элементы при фиксации
				 */
				aceEditorHelpButtons.parentNode.style.height = aceEditorHelpButtons.offsetHeight + "px";

				if ( !hasClass( aceEditorPre.parentNode , "ace_editor_full_window" ) )
					aceEditorHelpButtons.style.width = aceEditorPre.offsetWidth + "px";

				addClass( aceEditorHelpButtons , className );
			}
		else
			if ( window.scrollY <= top || window.scrollY > editorButtonsMaxY - aceEditorHelpButtons.offsetHeight )
				{
					removeClass( aceEditorHelpButtons , className );
					aceEditorHelpButtons.style.width = "";
				}




		/**
		 * Статусная строка редактора
		 */
		var aceStatusBar = document.getElementById( "aceStatusBar" );

		top = aceStatusBar.parentNode.parentNode.offsetTop + aceStatusBar.parentNode.offsetTop;

		var windowBottomOffset = window.scrollY + window.innerHeight - aceStatusBar.offsetHeight;

		if ( hasClass( aceEditorPre.parentNode , "ace_editor_full_window" ) || (top >= windowBottomOffset && windowBottomOffset >= editorTop) )
			{
				/**
				 * Задать блоку высоту, чтобы не съезжали элементы при фиксации
				 */
				aceStatusBar.parentNode.style.height = aceStatusBar.offsetHeight + "px";

				if ( !hasClass( aceEditorPre.parentNode , "ace_editor_full_window" ) )
					aceStatusBar.style.width = aceEditorPre.offsetWidth + "px";

				addClass( aceStatusBar , className );
			}
		else
			if ( windowBottomOffset > top || windowBottomOffset < editorTop )
				{
					removeClass( aceStatusBar , className );
					aceStatusBar.style.width = "";
				}



		return true;
	};

aceEditorsOnScreen();

// При прокрутке окна:
window.onscroll = aceEditorsOnScreen;
// При прокрутке окна:
window.onresize = aceEditorsOnScreen;





// При смене темы:
//editor.on( "changeTheme" , function ()
//{
//	alert( "theme" );
//	themesList.value = editor.getTheme().split( '/' ).pop();
//} );



// Хранить изначальное название страницы:
var pageTitle = document.title;

// Добавить в статусную строку информацию об обработке документа:
var aceStatusBar = document.getElementById( "aceStatusBar" );
aceStatusBar.parentNode.style.height = aceStatusBar.offsetHeight + "px";

// ReadOnly:
var readOnlyIMG = document.createElement( 'img' );
//readOnlyIMG.id = "readOnlyIMG";
readOnlyIMG.src = aceJSurl + "/ace/images/lock.gif";
// В каком состоянии сейчас:
//readOnlyIMG.className = editor.getReadOnly();

var count = 0;

while ( document.getElementById( 'aceDocReadOnly' + ++count ) )
	{
		var aceDocReadOnly = document.getElementById( 'aceDocReadOnly' + count );
		aceDocReadOnly.onclick = setReadOnly;

		var readOnlyIMGcopy = readOnlyIMG.cloneNode( true );
//		readOnlyIMGcopy.className = editor.getReadOnly();
		aceDocReadOnly.appendChild( readOnlyIMGcopy );

		var readOnlySpan = document.createElement( 'span' );
		readOnlySpan.innerHTML = " – ReadOnly";
		aceDocReadOnly.appendChild( readOnlySpan );
	}





var aceSaveButton = document.getElementById( "aceSaveButton" );
var aceTipografButton = document.getElementById( "aceTipografButton" );

var documentStatus = function ( message )
	{
		var stats =
			[
			];

		// Проверить текст на изменения:
		aceSaveButton.status = aceSaveButton.text == editor.getValue();


		if ( !aceSaveButton.sessionButton )
			{
				removeClass(ace.sesButton,"ace-selected");

				/**
				 * Отметит, что данный докуент сейчас отображён
				 */
				aceLoadSession.call( ace.sesButton );
			}


		if ( aceSaveButton.startText == editor.getValue() && !aceSaveButton.text )
			{
				removeClass( aceSaveButton , ["yellow", "green"] );
				removeClass( aceSaveButton.sessionButton , ["yellow", "green"] );
			}

		else
			{
				if ( aceSaveButton.status != true )
					{
						removeClass( aceSaveButton , "green" );
						removeClass( aceSaveButton.sessionButton , "green" );

						addClass( aceSaveButton , "yellow" );
						addClass( aceSaveButton.sessionButton , "yellow" );

						stats.push( "Изменения не сохранены" );
					}
				else
					{
						removeClass( aceSaveButton , "yellow" );
						removeClass( aceSaveButton.sessionButton , "yellow" );

						addClass( aceSaveButton , "green" );
						addClass( aceSaveButton.sessionButton , "green" );
					}
			}





		if ( window.aceTipografButton )
			{

				aceTipografButton.status = aceTipografButton.text == editor.getValue();


				if ( editor.getValue() == "" || (
					!aceTipografButton.text && aceTipografButton.startText == editor.getValue()
					) )
					removeClass( aceTipografButton , ["yellow", "green"] );

				else
					{
						// Статус отпиграфированности текста:
						if ( aceTipografButton.status != true )
							{
								removeClass( aceTipografButton , "green" );
								addClass( aceTipografButton , "yellow" );

								stats.push( "Текст не ОтТипографированЪ" );
							}
						else
							{
								removeClass( aceTipografButton , "yellow" );
								addClass( aceTipografButton , "green" );
							}
					}
			}




		/**
		 * Yandex Speller Check
		 */
		if ( window.aceYandexSpellerButton )
			{
				// Статус сохранения изменений в тексте:
				if ( editor.getValue() == "" )
					removeClass( aceYandexSpellerButton , ["green", "yellow", "red"] );
				/**
				 * Иначе, если текст не равен начальному и есть текст
				 */
				else
					{
						/**
						 * Если есть ошибки
						 */
						if ( aceYandexSpellerButton.status && aceYandexSpellerButton.markers.length )
							{
								removeClass( aceYandexSpellerButton , ["yellow", "green"] );
								addClass( aceYandexSpellerButton , "red" );
								stats.push( "Есть ошибки" );
							}
						else
							{
								/**
								 * Если текст проверен и нет ошибок
								 */
								if ( aceYandexSpellerButton.text == editor.getValue() )
									{
										removeClass( aceYandexSpellerButton , ["yellow", "red"] );
										addClass( aceYandexSpellerButton , "green" );
									}
								/**
								 * Если не проверен
								 */
								else
									{
										removeClass( aceYandexSpellerButton , ["green", "red"] );
										addClass( aceYandexSpellerButton , "yellow" );
										stats.push( "Не проверен" );
									}
							}
					}
			}


		// + переданное сообщение:
		if ( message && message != "" )
			stats.push( message );

		// Если нет сообщений, то добавить:
		if ( stats.length == 0 )
			stats.push( "Нет никаких изменений" );


		document.title = stats.join( " || " ) + " – " + pageTitle;
		aceStatusBar.title = stats.join( " || " );

	};

// При изменениях текста проверять и выводить состояние:
editor.session.doc.on( "change" , function () {
	documentStatus();
} );

// Отобразить сообщение:
//documentStatus( "Текст загружен" );


function aceAutoSizeOnOff ()
	{
		if ( hasClass(aceEditorPre.parentNode,"ace_editor_full_window") )
			return false;

		if ( editor.getOption( "maxLines" ) /*|| editor.getOption("autoScrollEditorIntoView")*/ )
			{
				/*editor.setOption("autoScrollEditorIntoView",false);*/
				editor.setOption( "minLines" , false );
				editor.setOption( "maxLines" , false );

				var height = window.innerHeight * 0.85;

				document.getElementById( "aceEditorMainWindow" ).style.height = height + "px";
			}
		else
			{
				/*editor.setOption("autoScrollEditorIntoView",true);*/
				editor.setOption( "minLines" , 15 );
				editor.setOption( "maxLines" , 10000 );
			}

		// Переразмерить:
		editor.resize();

		return true;

	}

