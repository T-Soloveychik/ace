/**
 * Created by Created by PhpStorm.
 *
 * @file This is my cool script.
 *
 * Детальное описание
 *
 * @example
 * <xmp id="aceText-1" data-ace-lang="html" data-ace-docName="summary" style="display: none;">html Text</xmp>
 *
 * <div id="aceEditorWindow"></div>
 *
 * <script type="text/javascript" src="/test/tim/ssh/ace/one-window.js"></script>
 *
 * @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
 *
 * @version 2014-08-09 22:09
 *
 * @copyright Timofey - 1september 2014
 *
 * @TODO: Sessions
 * @TODO: Get Sessions by HTML id elems - "aceTextToEdit-1" / data-ace-lang / data-ace-...
 * @TODO: Color status of buttons redactor-height
 * @TODO: Diff help buttons to langs
 */


function aceAddJS ( src , onload )
	{
		var id = src.split("/").pop();

		if ( document.getElementById(id) )
			{
				if ( onload )
					onload();

				return true;
			}

		var putTo = document.getElementsByTagName( "head" )[0] || document.getElementsByTagName( "body" )[0];

		var JS = document.createElement( "script" );

//		JS.type = "application/javascript";
		JS.type = "text/javascript";

		if ( onload )
			JS.onload = onload;
		JS.src = src;

//		JS.setAttribute("defer","");

		putTo.appendChild( JS );

		return true;
	}


function aceAddCSS ( href )
	{
		var id = href.split("/").pop();

		if ( document.getElementById(id) )
			return true;

		var putTo =
			document.getElementsByTagName( "head" )[0]
				|| document.getElementsByTagName( "body" )[0];

		var JS = document.createElement( "link" );

		JS.type = "text/css";
		JS.rel = "stylesheet";

		JS.href = href;

		putTo.appendChild( JS );

		return true;
	}


var aceJSurl = "//api.1september.ru";

if ( /\/test\/tim\/ssh\//.test(location.pathname) )
	aceJSurl = "/test/tim/ssh";



aceAddCSS( aceJSurl + "/ace/editor-style.css" );



if ( document.readyState != "complete" )
	{
		aceAddJS( aceJSurl + "/ace/ajax.js" , aceAddBuilder );
		aceAddJS( aceJSurl + "/ace/general.js" , aceAddBuilder );
	}


function aceAddBuilder ()
	{
		if ( window.aceAddBuilderSet )
			return false;

		window.aceAddBuilderSet = true;

		aceAddJS( aceJSurl + "/ace-builds/src/ace.js" , aceAddHTMLJS );

		return true;

	}

function aceAddHTMLJS ()
	{
		aceAddJS( aceJSurl + "/ace/one-window-html.js" , aceAddOptionsJS );
	}

function aceAddOptionsJS ()
	{
		if ( window.aceAddOptionsJSSet )
			return false;

		window.aceAddOptionsJSSet = true;

		aceAddHTML();



		aceAddJS( aceJSurl + "/ace-builds/src/ext-settings_menu.js" );
		aceAddJS( aceJSurl + "/ace-builds/src/ext-language_tools.js" );
		aceAddJS( aceJSurl + "/ace-builds/src/ext-keybinding_menu.js" );

		aceAddJS( aceJSurl + "/ace-builds/src/ext-spellcheck.js" );

		aceAddJS( aceJSurl + "/ace-builds/src/ext-modelist.js" );

		/**
		 * Emmet
		 * load emmet code and snippets compiled for browser
		 */
		aceAddJS( "https://nightwing.github.io/emmet-core/emmet.js" );
		aceAddJS( aceJSurl + "/ace-builds/src/ext-emmet.js" );


		aceAddJS( aceJSurl + "/ace/options/statusbar.js" );


		aceAddJS( aceJSurl + "/ace/options/functions.js" , aceInitEditor );

		return true;

	}

function aceInitEditor ()
	{
//		console.log('window.acePrinted: ' + (window.acePrinted));


		if ( window.acePrinted )
			return false;

		try
			{
//				var text = document.createTextNode( document.readyState );
//				document.getElementsByTagName( "body" )[0].appendChild( text );
//				document.getElementsByTagName( "body" )[0].appendChild( document.createTextNode("\n<br>") );

				// Запускаем:
				window.editor = ace.edit( "aceEditorMainWindow" );

				editor.on('mousemove', function(e) {
					if ( !editor.isFocused() )
						editor.focus();
				});

				ace.require( 'ace/ext/settings_menu' ).init( editor );

				ace.require( 'ace/ext/keybinding_menu' ).init( editor );

				var StatusBar = ace.require( "ace/ext/statusbar" ).StatusBar;

				// create a simple selection status indicator
				var statusBar = new StatusBar( editor , document.getElementById( "aceDocStatus" ) );

			}
		catch ( e )
			{
//				console.log(e);
				window.clearTimeout( window.aceEnd );
				return window.aceEnd = setTimeout( aceInitEditor , 150 );
			}


		window.acePrinted = true;


		// trigger extension - autocompletion.html
		ace.require( "ace/ext/language_tools" );


		aceAddJS( aceJSurl + "/ace/options/settings.js" , aceGetAndSetTexts );


		return true;
	}

function aceGetAndSetTexts()
	{
		var number = 0;

		while ( document.getElementById("aceText-" + ++number) )
			{
				var textElem = document.getElementById("aceText-" + number);

				var sesName = textElem.getAttribute("data-ace-docName");

				var text = textElem.innerHTML;

				var lang = textElem.getAttribute("data-ace-lang");

				ace.sesButton = aceNewSession(sesName,text,lang);
			}

		/**
		 * Отметит, что данный документ сейчас отображён
		 */
		aceLoadSession.call( ace.sesButton );


		aceAddNewSessionButton();


		aceAddHelpersJS();

	}


function aceAddHelpersJS ()
	{
//		console.log('window.aceEnd: ' + (window.aceEnd));

		if ( !ace || !editor.getTheme() )
			{
				window.clearTimeout( window.aceEnd );
				return window.aceEnd = setTimeout( aceAddHelpersJS , 350 );
			}

		aceAddJS( aceJSurl + "/ace/options/options.js" );

		aceAddJS( aceJSurl + "/ace/options/helpers.js" );

		aceAddJS( aceJSurl + "/ace/options/saving.js" );

		aceAddJS( aceJSurl + "/ace/options/tipograph.js" );

		aceAddJS( aceJSurl + "/ace/options/keys.js" );

		aceAddJS( aceJSurl + "/ace/yandex/speller.js" );

		aceAddJS( aceJSurl + "/ace/yandex/speller-functions.js" );



		if ( window.onLoadAceEditor )
			onLoadAceEditor();



		return true;

	}
