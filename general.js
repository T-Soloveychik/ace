/**
 * Created by Created by PhpStorm.
 *
 * file name general.js
 *
 * @brief Коротко – пояснение
 *
 * @details Детальное описание
 *
 * @author Timofey [tim] <timofey@1september.ru>
 * @author Timofey [tim] <T-Solovechik@ya.ru>
 *
 * @date 2014-06-26 11:44
 */

function setCookie ( name , value , options )
	{
		options = options || {};

		var expires = options.expires;

		if ( typeof expires == "number" && expires )
			{
				var d = new Date();
				d.setTime( d.getTime() + expires * 1000 );
				expires = options.expires = d;
			}
		if ( expires && expires.toUTCString )
			{
				options.expires = expires.toUTCString();
			}

		value = encodeURIComponent( value );

		var updatedCookie = name + "=" + value;

		for ( var propName in options )
			{
				updatedCookie += "; " + propName;
				if ( options.hasOwnProperty( propName ) )
					var propValue = options[propName];
				if ( propValue !== true )
					{
						updatedCookie += "=" + propValue;
					}
			}

		document.cookie = updatedCookie;
	}



// возвращает cookie с именем name, если есть, если нет, то undefined:
function getCookie ( name )
	{
		// Экранировать имя и попробовать найти среди кукенций:
		var matches = document.cookie.match( new RegExp(
			"(?:^|; )" + name.replace( /([\.$?*|{}\(\)\[\]\\\/\+^])/g , '\\$1' ) + "=([^;]*)"
		) );
		// Если нашлась, то вернуть содержание, вторые скобки:
		return matches ? decodeURIComponent( matches[1] ) : undefined;
	}



// Если класса нет, то добавляет, если есть –  убирает:
function addRemoveClass ( elem , name )
	{
		name = name.trim();
		// Убрать ненужные пробелы:
		var regForSpaces = new RegExp( getStringForRegExpUselessSpaces() , "mig" );
		elem.className = elem.className.replace( regForSpaces , "$1" );

		var classReg = new RegExp( "(^|\\s)" + name + "(?=(\\s|$))" , "mig" );
		if ( classReg.test( elem.className ) )
			elem.className = elem.className.replace( classReg , "" );
		else
			{
				// Если есть другие, то добавить:
				if ( elem.className )
					elem.className = elem.className + " " + name;
				// Иначе просто указать:
				else elem.className = name;
			}
	}



// Добавить класс:
function addClass ( elem , name , many )
	{
		name = name.trim();


		// Убрать ненужные пробелы:
		var regForSpaces = new RegExp( getStringForRegExpUselessSpaces() , "mig" );
		elem.className = elem.className.replace( regForSpaces , "$1" );

		/**
		 * Если есть другие классы, то добавить к ним
		 */
		if ( elem.className )
			{
				var classReg = new RegExp( "(^|\\s)" + name + "(?=(\\s|$))" , "mig" );

				/**
				 * Завершить
				 *
				 * если нет отметки, о том, что добавлять даже если есть,
				 *
				 * и класс уже есть, то завершить
				 */
				if ( many !== true && classReg.test( elem.className ) )
					return false;

				return elem.className = elem.className + " " + name;
			}
		/**
		 * Иначе просто указать
		 */
		else return elem.className = name;
	}



// Убрать класс:
function removeClass ( elem , names )
	{
		name = name.trim();
		// Убрать ненужные пробелы:
		var regForSpaces = new RegExp( getStringForRegExpUselessSpaces() , "mig" );
		elem.className = elem.className.replace( regForSpaces , "$1" );

		var forCheck = elem.className;

		if ( typeof names == 'string' )
			names = [names];

		for ( var i = 0 ; i < names.length ; i++ )
			{
				var classReg = new RegExp( "(^|\\s)" + names[i] + "(?=(\\s|$))" , "mig" )

				if ( classReg.test( elem.className ) )
					elem.className = elem.className.replace( classReg , "" );
			}


		/**
		 * Если были изменения, то вернуть className
		 */
		if ( forCheck != elem.className )
			return elem.className;


		return false;
	}



/**
 * Проверка наличия HTML классов у элемента
 *
 * Если не будет найден один из переданных классов, то вернёт false
 *
 * @param {element} elem DOM объект у которого будет проверяться наличие класса(ов)
 *
 * @param {string|string[]} names название класса или массив с названиями
 *
 * @return {boolean} есть отсутствует один из переданных классов, то false, если все есть, то true
 */
function hasClass ( elem , names )
	{
		if ( typeof names == 'string' )
			names = [names];

		for ( var i = 0 ; i < names.length ; i++ )
			{
				var classReg = new RegExp( "(^|\\s)" + names[i] + "(?=(\\s|$))" , "mig" )

				if ( !classReg.test( elem.className ) )
					return false;
			}

		return true;
	}


/**
 * Возвращает строку для создания регулярного выражения, с пустой скобкой сравнения, для замены на $1
 *
 * @example
 * var regExp = new RegExp(getStringForRegExpUselessSpaces(),"mig")
 * string.replace(regExp , "$1")
 *
 * @author Timofey <a href="mailto:timofey@1september.ru">timofey@1september.ru</a>/<a href="mailto:T-Solovechik@ya.ru">T-Solovechik@ya.ru</a>
 *
 * @version 2014-08-7 15:23
 *
 * @returns {string} строка с набор для поиска по регулярному выражению
 */
function getStringForRegExpUselessSpaces ()
	{
		var regParts = [
			'(\\S)[\\t\\ ]+(?=[\\t\\ ]|$)' // пробелы перед пробельными символами, следующие за НЕ пробельным символом (не в начале строки)
			, '^[\\t\\ ]+(?=$)' // строки только из пробельных символов
//			, '[\\t\\ ]+(?=$)' // пробельные символы в конце строки
			, '\\ +(?=\\t)' // пробельные символы между табами
			, '\\v+' // вертикальные отступы
		];

		regParts = regParts.join( "|" );

		return regParts;
	}




// Меняем адрес (HTML5):
//history.pushState( null, null, this.href );
//
//// Ловим нажатия назад/вперёд:
//window[ window.addEventListener ? 'addEventListener' : 'attachEvent' ]( 'popstate', function( event )
//	{
//		// получение location из объекта window.history
//		var loc = history.location || document.location;
//
//		alert( "return to: " + loc );
//
//	}, false);



//(function(){ var selected; if (window.getSelection) selected = window.getSelection(); else if (document.getSelection) selected = document.getSelection(); else selected = document.selection.createRange().text; var q = ''+selected; if (q) location='http://www.google.com/search?q='+encodeURIComponent(q);})();
